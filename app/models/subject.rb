class Subject < ApplicationRecord
  has_many :contents

  validates :key, presence: { message: I18n.t("validations.cant_be_blank") }
end
