class About < ApplicationRecord
  belongs_to :locale

  validates :body, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :locale_id, presence: { message: I18n.t("validations.cant_be_blank") }

  def self.search_query(params)
    abouts = About.arel_table

    q = abouts.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if About.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(abouts[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(abouts[:id])
        q.order(abouts[:id].desc)
      end
    end
    # q.where(abouts[:locale_id].matches("%#{params[:locale_id]}%")) if params[:locale_id].present?
    # q.where(abouts[:body].matches("%#{params[:body]}%")) if params[:body].present?

    q
  end
end
