class Role < ApplicationRecord
  has_many :users
  
  NAMES = [:member, :manager, :admin]

  class << self
    NAMES.each do |name_constant|
      define_method("get_#{name_constant}") { where(name: name_constant.to_s).first_or_create }
    end

    def search_query(params)
      roles = Role.arel_table

      if params[:count]
        q = roles.project("COUNT(*)")
      else
        q = roles.project(Arel.star)
        if Role.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          q.order(roles[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
        else
          q.group(roles[:id])
          q.order(roles[:id].desc)
        end
      end

      q.where(roles[:name].matches("%#{params[:name]}%")) if params[:name].present?

      q
    end
  end
end
