class CurrentLocation < ApplicationRecord

  belongs_to :user
  belongs_to :country

  validate :reservations_must_not_overlap, on: :update

  class << self
    def search_query(params)
      current_locations = CurrentLocation.arel_table

      q = current_locations.project(params[:count] ? "COUNT(*)" : Arel.star)

      if params[:count]
      else
        if CurrentLocation.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          q.order(current_locations[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
        else
          q.group(current_locations[:id])
          q.order(current_locations[:id].desc)
        end
      end

      puts params[:user_id]
      q.where(current_locations[:city].matches("%#{params[:city]}%")) if params[:city].present?
      q.where(current_locations[:from].gteq(DateTime.parse(params[:from]))) if params[:from].present?
      q.where(current_locations[:to].lteq(DateTime.parse(params[:to]))) if params[:to].present?
      q.where(current_locations[:user_id].eq(params[:user_id]))
      q
    end
  end

  private

  def reservations_must_not_overlap
    if active_changed? && active
      errors.add(:base, 'Overlapping reservation exists') if self.to < self.from

      return if self.class
                  .where(active: true)
                  .where.not(id: self.id)
                  .where(user_id: self.user_id)
                  .where("current_locations.from < ? AND current_locations.to > ?", self.to, self.from)
                  .none?

      errors.add(:base, 'Overlapping reservation exists')
    end
  end
end