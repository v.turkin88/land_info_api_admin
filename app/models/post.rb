class Post < ApplicationRecord
  translates :title, :description

  belongs_to :user

  enum status: {
    draft: 0,
    published: 1,
    archive: 2
  }

  def self.search_query(params)
    news = Post.arel_table

    q = news.project(params[:count] ? "COUNT(*)" : Arel.star)

    if !params[:count]
      if Post.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(news[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(news[:id])
        q.order(news[:id].desc)
      end
    end

    q.where(news[:user_id].eq(params[:user_id])) if params[:user_id].present?

    q
  end
end
