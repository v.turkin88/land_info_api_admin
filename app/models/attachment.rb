class Attachment < ApplicationRecord
  include ApplicationHelper

  belongs_to :attachable, polymorphic: true, optional: true

  enum attachment_type: {
    video: 1,
    audio: 2,
    photo: 3
  }

  validates :name, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :attachment_type, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :user_id, presence: { message: I18n.t("validations.cant_be_blank") }

end