class Translation < ApplicationRecord

  belongs_to :locale
  belongs_to :content_key

  validates :title, presence: {message: I18n.t('validations.cant_be_blank') }

  def self.search_query(params)
    translations = Translation.arel_table
  
    q = translations.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Translation.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(translations[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.group(translations[:id])
        q.order(translations[:id].desc)
      end
    end

    q.where(translations[:title].matches("%#{params[:title]}%")) if params[:title].present?
    q.where(translations[:locale_id].matches("%#{params[:locale_id]}%")) if params[:locale_id].present?
    q.where(translations[:content_key_id].matches("%#{params[:content_key_id]}%")) if params[:content_key_id].present?
    q.where(translations[:active].eq(params[:active].kind_of?(TrueClass))) if params[:active].present?
  
    q
  end
end

