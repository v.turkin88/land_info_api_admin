class ContentKey < ApplicationRecord

  has_many :translations

  validates :key, presence: {message: I18n.t('validations.cant_be_blank') }

  def self.search_query(params)
    content_keys = ContentKey.arel_table

    q = content_keys.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if ContentKey.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(content_keys[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.group(content_keys[:id])
        q.order(content_keys[:id].desc)
      end
    end

    q.where(content_keys[:key].matches("%#{params[:key]}%")) if params[:key].present?

    q
  end
end

