class Content < ApplicationRecord

  belongs_to :locale
  belongs_to :content_key

  validates :data, presence: {message: I18n.t('validations.cant_be_blank') }

  def self.search_query(params)
    contents = Content.arel_table
    content_key = Arel::Table.new(:content_keys)

    q = contents.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Content.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(contents[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.group(contents[:id], content_key[:id])
        q.order(contents[:id].desc)
      end
    end

    q.where(contents[:data].matches("%#{params[:data]}%")) if params[:data].present?
    q.where(contents[:locale_id].matches("%#{params[:locale_id]}%")) if params[:locale_id].present?
    q.where(contents[:content_key_id].matches("%#{params[:content_key_id]}%")) if params[:content_key_id].present?
    q.where(contents[:active].eq(params[:active].kind_of?(TrueClass))) if params[:active].present?
    q.join(content_key, Arel::Nodes::OuterJoin).on(contents[:content_key_id].eq(content_key[:id]))

    q
  end


  def to_json
    {
      data: data,
      key: content_key.key,
      locale: locale.key
    }
  end

end

