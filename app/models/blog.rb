class Blog < ApplicationRecord
  include ApplicationHelper

  belongs_to :user

  enum status: {
    draft: 0,
    active: 1,
    private_blog: 2
  }

  class << self
    def search_query(params)
      blogs = Blog.arel_table
    
        q = blogs.project(params[:count] ? "COUNT(*)" : Arel.star)
    
        if params[:count]
        else
          if Blog.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
            q.order(blogs[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
          else
            q.group(blogs[:id])
            q.order(blogs[:id].desc)
          end
        end
    
        q.where(blogs[:name].matches("%#{params[:name]}%")) if params[:name].present?
        q.where(blogs[:status].eq(params[:status])) if params[:status].present?
        q.where(blogs[:content].matches("%#{params[:content]}%")) if params[:content].present?
        q.where(blogs[:user_id].eq(params[:user_id])) if params[:user_id].present?
        q.where(blogs[:status].eq(:active)) if params[:only_status_active]
        q
    end
  end
end