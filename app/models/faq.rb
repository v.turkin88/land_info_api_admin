class Faq < ApplicationRecord
  translates :title, :description

  def self.search_query(params)
    faqs = Faq.arel_table

    q = faqs.project(params[:count] ? "COUNT(*)" : Arel.star)

    if p!arams[:count]
      if Faq.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(faqs[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(faqs[:id])
        q.order(faqs[:id].desc)
      end
    end

    q.where(faqs[:locale_id].eq(params[:locale_id])) if params[:locale_id].present?
    q.where(faqs[:role_id].eq(params[:role_id])) if params[:role_id].present?

    q
  end
end
