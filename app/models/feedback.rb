class Feedback < ApplicationRecord
  belongs_to :user, required: false

  validates :email, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :message, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :full_name, presence: { message: I18n.t("validations.cant_be_blank") }

  def self.search_query(params)
    feedbacks = Feedback.arel_table

    q = feedbacks.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Feedback.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(feedbacks[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(feedbacks[:id])
        q.order(feedbacks[:id].desc)
      end
    end

    q.where(
      feedbacks[:email].matches("%#{params[:search]}%")
        .or(feedbacks[:full_name].matches("%#{params[:search]}%"))
    ) if params[:search].present?

    q
  end
end
