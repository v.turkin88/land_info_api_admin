class PrivacyPolicy < ApplicationRecord
  belongs_to :locale

  validates :body, presence: {message: I18n.t('validations.cant_be_blank') }
  validates :locale_id, presence: {message: I18n.t('validations.cant_be_blank') }

  def self.search_query(params)
    privacy_policies = PrivacyPolicy.arel_table

    q = privacy_policies.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if PrivacyPolicy.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(privacy_policies[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.group(privacy_policies[:id])
        q.order(privacy_policies[:id].desc)
      end
    end

    q.where(privacy_policies[:body].matches("%#{params[:body]}%")) if params[:body].present?

    q
  end
end

