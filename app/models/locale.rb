class Locale < ApplicationRecord
  has_many :content
  has_many :about
  has_many :faq
  has_many :translations
  has_many :privacy_policies

  validates :title, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :iso2, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :iso3, presence: { message: I18n.t("validations.cant_be_blank") }

  def self.search_query(params)
    locales = Locale.arel_table

    q = nil

    if params[:count]
      q = locales.project("COUNT(*)")
    else
      q = locales.project(Arel.star).group(locales[:id]).order(locales[:iso2])
    end

    q.where(locales[:iso2].matches("%#{params[:iso2]}%")) if params[:iso2].present?
    q.where(locales[:iso3].matches("%#{params[:iso3]}%")) if params[:iso3].present?

    q.where(locales[:active].eq(params[:active].kind_of?(TrueClass))) if params[:active].present?

    q
  end

  def self.locale(locale)
    t = Locale.arel_table

    locales = Locale.where(t[:iso2].eq(locale).or(t[:title].eq(locale)).or(t[:iso3].eq(locale)))

    locale = locales.first if locales.size == 1
    locale = Locale.locale("eng") if !locale.present?
    locale
  end
end
