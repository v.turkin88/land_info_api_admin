class GeneralLocation < ApplicationRecord
  belongs_to :user
  belongs_to :country

  validates :city, presence: { message: I18n.t("validations.cant_be_blank") }
end
