class BusinessType < ApplicationRecord
  has_many :users

  validates :content_key_id, presence: true

  scope :only_active, -> { where(active: true) }

  class << self
    def search_query(params)
      business_types = BusinessType.arel_table

      q = business_types.project(params[:count] ? "COUNT(*)" : Arel.star)

      if params[:count]
      else
        if BusinessType.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          q.order(business_types[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
        else
          q.group(business_types[:id])
          q.order(business_types[:id].desc)
        end
      end
      q
    end
  end

end