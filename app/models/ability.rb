# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.admin?
      can :manage, :all
    elsif user.business?
      business_abilities(user)
    elsif user.artist?
      artist_abilities(user)
    elsif user.producer?
      producer_abilities(user)
    else
      can :index, Role
      can :index, Locale
      can :index, About
      can :index, Faq
      can :create, Feedback
      can :index, Subject
      can :index, Ganre
      can :index, PrivacyPolicy
      can :manage, GeneralLocation
    end
  end

  def business_abilities(user)
    can :index, Role
    can :index, Locale
    can :index, About
    can :index, Faq
    can :create, Feedback
    can :index, Subject
    can :index, Ganre
    can :index, PrivacyPolicy
    can :index, BusinessType
    can :read, BusinessType
    can :manage, GeneralLocation
    can :manage, CurrentLocation
    can :manage, Event
    can :manage, Gallery
    can :manage, Attachment
    can :read, Event
    can :manage, Blog
  end

  def artist_abilities(user)
    can :index, Role
    can :index, Locale
    can :index, About
    can :index, Faq
    can :create, Feedback
    can :index, Subject
    can :index, Ganre
    can :index, PrivacyPolicy
    can :manage, GeneralLocation
    can :manage, CurrentLocation
    can :read, Event
    can :manage, Gallery
    can :manage, Attachment
    can :manage, Blog
  end

  def producer_abilities(user)
    can :index, Role
    can :index, Locale
    can :index, About
    can :index, Faq
    can :create, Feedback
    can :index, Subject
    can :index, Ganre
    can :index, PrivacyPolicy
    can :manage, CurrentLocation
    can :manage, GeneralLocation
  end
end
