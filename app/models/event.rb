class Event < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :users

  validates :title, presence: { message: I18n.t("validations.cant_be_blank") }

  has_many :attachments, as: :attachable, dependent: :destroy

  enum status: {
    draft: 1,
    activated: 2,
    started: 3,
    completed: 4,
    deleted: 5
  }

  class << self
    def search_query(params)
      events = Event.arel_table
      events_users = Arel::Table.new(:events_users)

      if params [:count]
        q = events.project('COUNT(*)')
      else
        q = events.group(events[:id])
              .project('events.*',
               "(
                SELECT json_agg(t)
                  FROM (
                    SELECT
                      users.id,
                      users.first_name,
                      users.last_name,
                      users.nickname,
                      users.role_id,
                      users.email
                    FROM users
                    JOIN events_users ON events_users.user_id = users.id
                    JOIN roles ON role.id = users.role_id
                    WHERE events_users.event_id = events.id
                  ) t
                )"
              )
      end

      q = events.project(params[:count] ? "COUNT(*)" : Arel.star)

      if params[:count]
      else
        if Event.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          q.order(events[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
        else
          q.group(events[:id])
          q.order(events[:id].desc)
        end
      end

      q.where(events[:title].matches("%#{params[:title]}%")) if params[:title].present?
      q.where(events[:status].eq(params[:status])) if params[:status].present?
      q.where(events[:from].gteq(DateTime.parse(params[:from]))) if params[:from].present?
      q.where(events[:to].lteq(DateTime.parse(params[:to]))) if params[:to].present?
      q.where(events[:user_id].eq(params[:user_id])) if params[:user_id].present?
      q
    end
  end
end