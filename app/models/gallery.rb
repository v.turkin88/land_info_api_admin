class Gallery < ApplicationRecord

  belongs_to :parent, :class_name => "Gallery", optional: true

   belongs_to :user
   
   has_many :attachments, as: :attachable, dependent: :destroy

   validates :name, :description, presence: true, if: Proc.new { |parent| !parent_id.blank? }

  class << self
    def search_query(params)
      galleries = Gallery.arel_table
    
        q = galleries.project(params[:count] ? "COUNT(*)" : Arel.star)
    
        if params[:count]
        else
          if Gallery.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
            q.order(galleries[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
          else
            q.group(galleries[:id])
            q.order(galleries[:id].desc)
          end
        end
    
        q.where(galleries[:name].matches("%#{params[:name]}%")) if params[:name].present?
        q.where(galleries[:description].matches("%#{params[:description]}%")) if params[:description].present?
        q.where(galleries[:user_id].eq(params[:user_id])) if params[:user_id].present?
        q.where(galleries[:is_public].eq(true)) if params[:only_public]
        q
    end
  end
end