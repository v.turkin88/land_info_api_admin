class Country < ApplicationRecord

  has_many :users
  has_many :general_locations

  validates :name, presence: true
  validates :phone_code, presence: true
  validates :alpha2_code, presence: true
  validates :alpha3_code, presence: true
  validates :numeric_code, presence: true

  class << self
    def search_query(params)
      countries = Country.arel_table

      q = countries.project(params[:count] ? "COUNT(*)" : Arel.star)

      if params[:count]
      else
        if Country.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          q.order(countries[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
        else
          q.group(countries[:id])
          q.order(countries[:id].desc)
        end
      end

      q.where(countries[:name].matches("%#{params[:name]}%")) if params[:name].present?
      q.where(countries[:alpha2_code].matches("%#{params[:alpha2_code]}%")) if params[:alpha2_code].present?
      q.where(countries[:alpha3_code].matches("%#{params[:alpha3_code]}%")) if params[:alpha3_code].present?
      q.where(countries[:numeric_code].matches("%#{params[:numeric_code]}%")) if params[:numeric_code].present?
      q.where(countries[:phone_code].matches("%#{params[:phone_code]}%")) if params[:phone_code].present?
      q.where(countries[:selected].eq(true)) if params[:selected].present?
      q.where(countries[:selected].eq(params[:selected])) if params[:selected].present?

      q
    end

  end

  def self.country(locale)
    t = Country.arel_table

    locales = Country.where(t[:name].eq(locale).or(t[:alpha2_code].eq(locale)).or(t[:alpha3_code].eq(locale)).or(t[:numeric_code].eq(locale)))

    locale = locales.first if locales.size == 1
    locale = Country.country("US") if !locale.present?
    locale
  end
end
