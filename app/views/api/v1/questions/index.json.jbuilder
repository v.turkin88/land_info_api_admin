json.questions @questions.each do |question|
  json.extract! question, :id, :title, :description, :status, :created_at, :updated_at
end
json.count @count