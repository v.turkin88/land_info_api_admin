json.question do
  json.extract! @question, :id, :title, :description, :status, :created_at, :updated_at
end