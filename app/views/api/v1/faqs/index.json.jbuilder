json.faqs @faqs.each do |faq|
  json.extract! faq, :id, :title, :description, :locale
end

json.count @count
