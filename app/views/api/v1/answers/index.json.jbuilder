json.answers @answers.each do |answer|
  json.extract! answer, :id, :description, :status
end
json.count @count