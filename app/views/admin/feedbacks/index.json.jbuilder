json.feedbacks @feedbacks.each do |feedback|
  json.extract! feedback, :id, :email, :full_name, :message, :user
end