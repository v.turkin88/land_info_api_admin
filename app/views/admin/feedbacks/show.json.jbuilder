json.feedback do
  json.extract! @feedback, :id, :email, :full_name, :message, :user
  json.created_at @feedback.created_at.try :strftime, '%d-%m-%Y %H:%M'
end