json.content_keys @content_keys.each do |content_key|
  json.extract! content_key, :id, :key
end