json.posts @posts.each do |post|
  json.extract! post, :id, :active, :status, :user, :is_admin
  json.created_at post.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at post.updated_at.strftime("%d-%m-%Y %H:%M")

  json.translates post.translations.each do |translate|
    json.extract! translate, :locale, :title, :description
  end
end
