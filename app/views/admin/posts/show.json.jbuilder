json.post do
  json.extract! @post, :id, :active, :status, :is_admin, :user
  json.created_at @post.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at @post.updated_at.strftime("%d-%m-%Y %H:%M")

  json.translates @post.translations.each do |translate|
    json.extract! translate, :locale, :title, :description
  end
end
