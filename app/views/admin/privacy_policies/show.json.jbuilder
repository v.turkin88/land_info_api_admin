json.privacy_policy do
  json.extract! @privacy_policy, :id, :body, :document_url

  json.locale do
    json.extract! @privacy_policy.locale, :title, :iso2, :iso3, :id
  end if @privacy_policy.locale
end
