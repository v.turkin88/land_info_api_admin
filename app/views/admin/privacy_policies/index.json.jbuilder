json.privacy_policies @privacy_policies.each do |privacy_policy|
  json.extract! privacy_policy, :id, :body, :document_url
  json.locale privacy_policy.locale.title if privacy_policy.locale
end