json.countries @countries.each do |c|
  json.extract! c, :id, :phone_code, :name, :alpha3_code, :alpha2_code, :numeric_code, :selected
end
json.count @count