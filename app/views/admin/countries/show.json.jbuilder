json.country do
  json.extract! @country, :id, :phone_code, :name, :alpha3_code, :alpha2_code, :numeric_code, :selected
end