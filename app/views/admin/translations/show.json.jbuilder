json.translation do
  json.extract! @translation, :content_key_id, :locale_id, :active, :id, :title
  json.locale @translation.locale.title if @translation.locale
  json.content_key @translation.content_key.key if @translation.content_key
end