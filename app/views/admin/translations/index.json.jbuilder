json.translations @translations.each do |translation|
  json.extract! translation, :id, :content_key_id, :locale_id, :active, :title
  json.locale translation.locale.title if translation.locale
  json.content_key translation.content_key.key if translation.content_key
end