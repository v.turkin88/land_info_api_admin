json.abouts @abouts.each do |about|
  json.extract! about, :id, :body
  json.locale about.locale.title if about.locale
end
