json.about do
  json.extract! @about, :id, :body
  json.locale do
    json.extract! @about.locale, :title, :iso2, :iso3, :id
  end if @about.locale
end
