json.locale do
  json.extract! @locale, :iso2, :iso3, :id, :title
end
