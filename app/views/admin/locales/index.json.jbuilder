json.locales @locales.each do |locale|
  json.extract! locale, :id, :iso2, :iso3, :active, :title
end
