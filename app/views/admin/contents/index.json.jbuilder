json.contents @contents.each do |content|
  json.extract! content, :id, :data, :key, :content_key_id, :locale_id, :active
end