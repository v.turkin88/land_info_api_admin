json.content do
  json.extract! @content, :id, :data, :active
  json.content_key do
    json.extract! @content_key, :id, :key
  end
  json.locale do
      json.extract! @locale, :iso2, :iso3, :key, :id
  end
end