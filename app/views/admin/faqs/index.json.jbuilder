json.faqs @faqs.each do |faq|
  json.extract! faq, :id, :active
  json.created_at faq.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at faq.updated_at.strftime("%d-%m-%Y %H:%M")

  json.translates faq.translations.each do |translate|
    json.extract! translate, :locale, :title, :description
  end
end
