import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from "react-router";

import BaseLayout from "./components/layouts/Base";
import DashboardLayout from "./components/layouts";
import injectTapEventPlugin from 'react-tap-event-plugin';
import DashboardOverviewPage from "./components/pages/Dashboard/Overview";
import LoginPage from "./components/pages/Login";
import EmailSender from './components/system_settings/EmailSender';
import { store, history } from './create_store';
import { check } from './services/sessions';

// generated components paths
// import TermAndConditionForm from './components/term_and_condition/form';
import PrivacyPolicies from './components/privacy_policies/index';
import PrivacyPolicyForm from './components/privacy_policies/form';
import PrivacyPolicy from './components/privacy_policies/show';
import Admins from './components/admins/index';
import AdminForm from './components/admins/form';
import Admin from './components/admins/show';
import Feedbacks from './components/feedbacks/index';
import Feedback from './components/feedbacks/show';
// import Roles from './components/roles/index';
import Faqs from './components/faqs/index';
import FaqForm from './components/faqs/form';
import Faq from './components/faqs/show';
import Abouts from './components/abouts/index';
import AboutForm from './components/abouts/form';
import About from './components/abouts/show';

import Posts from './components/posts/index';
import PostForm from './components/posts/form';
import Post from './components/posts/show';

window.onload = function () {
  injectTapEventPlugin();
  ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route name="base" path="/" component={BaseLayout}>
        <Route name="dashboard" path='' component={DashboardLayout} onEnter={check}>
          <IndexRoute name="dashboard.overview" component={DashboardOverviewPage} />
          <Route name='email_sender' path='email_sender' component={EmailSender} />
          {/* <Route name='term_and_condition' path='term_and_condition' component={TermAndConditionForm} /> */}
          <Route name='users' path='users/:role' component={Admins} />
          <Route name='new_user' path='users/:role/new' component={AdminForm} />
          <Route name='edit_user' path='users/:role/:id/edit' component={AdminForm} />
          <Route name='show_user' path='users/:role/:id' component={Admin} />
          <Route name='feedbacks' path='feedbacks' component={Feedbacks} />
          <Route name='show_feedback' path='feedback/:id' component={Feedback} />
          <Route name='faqs' path='faqs' component={Faqs} />
          <Route name='new_faq' path='faq/new' component={FaqForm} />
          <Route name='edit_faq' path='faq/:id/edit' component={FaqForm} />
          <Route name='show_faq' path='faq/:id' component={Faq} />

          <Route name='privacy_policies' path='privacy_policies' component={PrivacyPolicies} />
          <Route name='privacy_policy' path='privacy_policy/new' component={PrivacyPolicyForm} />
          <Route name='privacy_policy' path='privacy_policy/:id/:edit' component={PrivacyPolicyForm} />
          <Route name='privacy_policy' path='privacy_policy/:id' component={PrivacyPolicy} />

          <Route name='abouts' path='abouts' component={Abouts} />
          <Route name='about' path='about/new' component={AboutForm} />
          <Route name='about' path='about/:id/:edit' component={AboutForm} />
          <Route name='about' path='about/:id' component={About} />

          {/* <Route name='roles' path='roles' component={Roles} /> */}
        
          <Route name='posts' path='posts' component={Posts} />
          <Route name='post' path='post/new' component={PostForm} />
          <Route name='post' path='post/:id/:edit' component={PostForm} />
          <Route name='post' path='post/:id' component={Post} />
        </Route>
        <Route name="login" path='login' component={LoginPage} />
      </Route>
      <Route path="*" component={LoginPage} />
    </Router>
  </Provider>,
  document.getElementById('content')
  );
};
