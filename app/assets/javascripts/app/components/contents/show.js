import React, {Component} from 'react';
import {connect} from 'react-redux';
import {FormGroup, ControlLabel, Row, Col} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
} from 'material-ui';

import {paperStyle} from '../common/styles';
import {show} from '../../services/contents';

class Content extends Component {
  state = {
    content: {
      content_type: '',
      key: '',
      body: '',
      data_type: ''
    },
  };

  componentWillMount() {
    this._retrieveContent();
  }

  _retrieveContent = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        content: res.content,
      })
    });
  };

  render() {
    const {content} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('content.label') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/contents' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <FormGroup>
          <Row>
            <Col sm={12}>
              <Row>
                <Col sm={6}>
                  <Row>
                    <Col sm={3}>
                      <ControlLabel>{ I18n.t('fields.type') }</ControlLabel>
                    </Col>
                    <Col sm={9}>
                      <span className="form-control-static">{content.content_type}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={3}>
                      <ControlLabel>{ I18n.t('fields.key') }</ControlLabel>
                    </Col>
                    <Col sm={9}>
                      <span className="form-control-static">{content.key}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={3}>
                      <ControlLabel>{ I18n.t('fields.body') }</ControlLabel>
                    </Col>
                    <Col sm={9}>
                      <span className="form-control-static">{content.body}</span>
                    </Col>
                  </Row>
                </Col>
                <Col sm={6}>
                  <Row>
                    <Col sm={3}>
                      <ControlLabel>{ I18n.t('fields.data_type') }</ControlLabel>
                    </Col>
                    <Col sm={9}>
                      <span className="form-control-static">{content.data_type}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={3}>
                      <ControlLabel>{ I18n.t('fields.created_at') }</ControlLabel>
                    </Col>
                    <Col sm={9}>
                      <span className="form-control-static">{content.created_at}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={3}>
                      <ControlLabel>{ I18n.t('fields.updated_at') }</ControlLabel>
                    </Col>
                    <Col sm={9}>
                      <span className="form-control-static">{content.updated_at}</span>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </FormGroup>

      </Paper>
    )
  }
}

export default connect(state => state)(Content)
