import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  SelectField,
  MenuItem
} from 'material-ui';
import {paperStyle} from '../common/styles';
import {show, upsert} from '../../services/contents';

class ContentForm extends Component {
  state = {
    content: {
      key: '',
      body: '',
      content_type: 'html',
      data_type:0,
      content:''
    },
    active: false,
    validationErrors: {}
  };

  componentWillMount() {
    this._retrieveContent();
  }

  _retrieveContent = () => {
    const {id} = this.props.params;
    if (!id) {
      this.setState({
        active: false
      })
      return false
    }
    show(id).success(res => {
      this.setState({
        content: res.content,
        active: true
      })
    })
  };

  _handleChange = (key, value) => {
    const {content} = this.state;
    this.setState({
      content: {
        ...content,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };

  handleSubmit = event => {
    event.preventDefault();
    const {content} = this.state;
    upsert(content)
      .success(res => {
        location.hash = '#/contents';
      })
      .progress(value => {
        this.setState({progress: value})
      })
  };

  render() {
    const {isLoading} = this.props.app.main;
    const {content, active, progress, validationErrors} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <h2>{ I18n.t('actions.new') }</h2>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/contents' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Row>
              <Col sm={4} smOffset={8} className="text-right">
                <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'}
                                  mode="determinate" value={progress} size={36}/>
                <RaisedButton type='submit' primary={true} className='pull-right' label="Save"
                              disabled={isLoading}/>
              </Col>
            </Row>
            <Row>
              <Col sm={2}>
                <ControlLabel>{ I18n.t('fields.type') }</ControlLabel>
              </Col>
              <Col sm={10}>
                <SelectField
                  floatingLabelText={ I18n.t('fields.type') }
                  value={content.content_type}
                  onChange={(_, __, val) => this._handleChange('content_type', val) }
                  disabled={ active }
                >
                  <MenuItem value={'about'} primaryText="About"/>
                  <MenuItem value={'events'} primaryText="Avents"/>
                  <MenuItem value={'footer'} primaryText="Footer"/>
                  <MenuItem value={'header'} primaryText="Header"/>
                  <MenuItem value={'subscribe'} primaryText="Subscribe"/>
                  <MenuItem value={'roles'} primaryText="Roles"/>
                  <MenuItem value={'contacts'} primaryText="Contacts"/>
                  <MenuItem value={'faqs'} primaryText="FAQ"/>
                  <MenuItem value={'filds'} primaryText="Filds"/>
                </SelectField>
              </Col>
            </Row>
            <Row>
              <Col sm={2}>
                <ControlLabel>
                  { I18n.t('fields.key') }
                </ControlLabel>
              </Col>
              <Col sm={10}>
                <TextField
                  hintText={ I18n.t('fields.key') }
                  fullWidth={true}
                  value={content.key}
                  onChange={(_, val) => this._handleChange('key', val) }
                  errorText={validationErrors.key}
                  disabled={ active }
                />
              </Col>
            </Row>
            <Row>
              <Col sm={2}>
                <ControlLabel>{ I18n.t('fields.data_type') }</ControlLabel>
              </Col>
              <Col sm={10}>
                <SelectField
                  floatingLabelText={ I18n.t('fields.data_type') }
                  value={content.data_type}
                  onChange={(_, __, val) => this._handleChange('data_type', val) }
                >
                  <MenuItem value={'html'} primaryText="HTML"/>
                  <MenuItem value={'string'} primaryText="String"/>
                </SelectField>
              </Col>
            </Row>
            <div style={{ display: content.data_type=='html' ? "block" : "none" }}>
            </div>
            <Row style={{ display: content.data_type=='string' ? "block" : "none" }}>
              <Col sm={2}>
                <ControlLabel>
                  { I18n.t('fields.body') }
                </ControlLabel>
              </Col>
              <Col sm={10}>
                <TextField
                  hintText={ I18n.t('fields.body') }
                  fullWidth={true}
                  value={content.content}
                  onChange={(_, val) => this._handleChange('content', val) }
                  errorText={validationErrors.body}
                />
              </Col>
            </Row>
          </FormGroup>
        </form>
      </Paper>

    )
  }
}

export default connect(state => state)(ContentForm)
