import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Row, Col} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  IconButton,
  Paper, RaisedButton,
} from 'material-ui';
import {
  ActionVisibility,
  ImageEdit,
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import SortingTh from '../common/sorting_th';
import Filters from '../common/filters_component';
import {paperStyle} from '../common/styles';
import {all, destroy} from '../../services/contents';

class Contents extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    contents: [],
    selectedRecord: {},
    count: 0
  };

  componentWillMount() {
    this._retrieveContents();
  }

  _retrieveContents = () => {
    const {filters} = this.state;
    all(filters).success(res => {
      this.setState({
        contents: res.contents,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveContents);
  };

  handleShowSizeChange = (_, per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveContents);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveContents)
  };

  handleDelete = () => {
    const {selectedRecord} = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveContents();
      this.closeConfirm();
    });
  };

  render() {
    const {contents, count} = this.state;
    const {page, per_page} = this.state.filters;
    const {palette} = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <h2>{ I18n.t('content.header') }</h2>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col sm={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10', '20', '50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col sm={4} className="text-right" style={{minHeight: 61}}>
            <RaisedButton href='#/content/new' className='pull-right'
                          primary={true} label={ I18n.t('actions.new') }
            />
          </Col>
        </Row>
        <Row>
          <Filters columns={[
            {label: 'Content type', key: 'content_type', type: 'string'},
            {label: 'Key', key: 'key', type: 'string'}
          ]} update={this.updateFilters}/>
        </Row>


        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters}column='content_type'>{ I18n.t('fields.type') }</SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters} column='key'>{ I18n.t('fields.key') }</SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('fields.data_type') }</TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('fields.body') }</TableHeaderColumn>
              <TableHeaderColumn><SortingTh update={this.updateFilters} column='created_at'>{ I18n.t('fields.created_at') }</SortingTh></TableHeaderColumn>
              <TableHeaderColumn>Actions</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              contents.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{item.content_type}</TableRowColumn>
                    <TableRowColumn>{item.key}</TableRowColumn>
                    <TableRowColumn>{item.data_type}</TableRowColumn>
                    <TableRowColumn>{item.body}</TableRowColumn>
                    <TableRowColumn>{item.created_at}</TableRowColumn>
                    <TableRowColumn className='text-right'>
                      <a href={`#/content/${item.id}`}>
                        <IconButton><ActionVisibility color={palette.primary1Color}/></IconButton>
                      </a>
                      <a href={`#/content/${item.id}/edit`}>
                        <IconButton><ImageEdit color={palette.accent1Color}/></IconButton>
                      </a>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <hr/>
      </Paper>
    )
  }
}

Contents.contextTypes = {
  muiTheme: React.PropTypes.object.isRequired
};

export default connect(state => state)(Contents)