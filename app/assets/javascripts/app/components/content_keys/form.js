import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix,
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  SelectField,
  AutoComplete,
  Toggle,
  MenuItem
} from 'material-ui';
import Image from '../common/image.component'
import {paperStyle} from '../common/styles';
import {show, upsert, all} from '../../services/content_keys';
import { FormErrorMessage } from '../common/form-error-message.component'

class ContentKeyForm extends Component {
  state = {
    content_key: {
      key: ''
    },
    validationErrors: {}
  };

  componentWillMount() {
    this._retrieveContentKey();
  }

  _retrieveContentKey = () => {
    const {id} = this.props.params;
    if (!id) {
      this.setState({
        active: false
      })
      return false
    }
    show(id).success(res => {
      this.setState({
        content_key: res.content_key,
        active: true
      })
    })
  };

  _handleChange = (key, value) => {
    const {content_key} = this.state;
    this.setState({
      content_key: {
        ...content_key,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };


  handleSubmit = event => {
    event.preventDefault();
    const {content_key} = this.state;
    upsert(content_key)
      .success(res => {
        location.hash = '#/content_keys';
      })
      .progress(value => {
        this.setState({progress: value})
      })
      .error(res => {
        this.setState({
        validationErrors: res.validation_errors
      })
    })
  };

  updateFlag = file => {
    this.setState({
      content_key: {
      ...this.state.content_key,
        flag: file
      }
    })
  };

  render() {
    const {isLoading} = this.props.app.main;
    const {content_key, progress, validationErrors } = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
      <Row>
        <Col sm={6}>
          <h2>{ content_key.id ? I18n.t('content_key.edit') : I18n.t('content_key.new') }</h2>
        </Col>
        <Col sm={6}>
          <RaisedButton
            href='#/content_keys'
            className='pull-right'
            secondary={true}
            label={ I18n.t('actions.back') }
          />
        </Col>
      </Row>

        <br/>
        <form onSubmit={this.handleSubmit}>

        <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('fields.key') }
              fullWidth={true}
              value={content_key.key}
              onChange={(_,val) => this._handleChange('key', val) }
              errorText={ (validationErrors.key || []).join(', ') }
            />
        </FormGroup>

        {/* <FormGroup>
              <FormErrorMessage errors={ validationErrors.selected  } />
            </FormGroup> */}

          <Col sm={4} smOffset={8} className="text-right">
    
            <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36}/>
            <RaisedButton type='submit'
            primary={true}
            className='pull-right'
            label={ I18n.t('actions.submit') }
            disabled={isLoading}/>
          </Col>
          
          <Clearfix />
        </form>
      </Paper>

    )
  }
}

export default connect(state => state)(ContentKeyForm)
