import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  Checkbox,
  CircularProgress
} from 'material-ui';
import SortingTh from '../common/sorting_th';
import {
  ImageEdit,
  ActionDelete,
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import Filters from '../common/filters_component';
import { paperStyle } from '../common/styles';
import {all, activate, destroy} from '../../services/content_keys'

class ContentKeys extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    content_keys: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveContentKeys();
  }

  _retrieveContentKeys = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        content_keys: res.content_keys,
        count: res.count
      });
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveContentKeys);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveContentKeys);
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveContentKeys)
  };

  _changeSelected = (id, isSelected) => {
    let content_key = {
      id: id
    };
    activate(content_key)
      .success(res => {
        this._retrieveContentKeys()
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
        this.setState({
          validationErrors: res.validation_errors
        });
      });
  };

  prepareToAction = (record, action) => {
    this.setState({
      selectedRecord: record,
      showConfirm: true,
      action: action
    })
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  handleDelete = () => {
    const {selectedRecord} = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveContentKeys();
      this.closeConfirm();
    });
  };

  handleAction = () => {
    const { selectedRecord, action } = this.state;
    switch (action) {
      case 'destroy':
        destroy(selectedRecord.id).success(res => {
          this._retrieveContentKeys();
          this.closeConfirm();
        });
        break;
    }
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { content_keys, count, showConfirm } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ul className="breadcrumb">
              <li>{ I18n.t('headers.content_key') }</li>
            </ul>
          </Col>
        </Row>

        <Row>
          <Col md={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10','20','50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col md={4}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36} />
            <RaisedButton
              href='#/content_key/new'
              className='pull-right'
              primary={true}
              label={ I18n.t('actions.new') }
            />
          </Col>
        </Row>

        <br/>
        <Filters columns={[
          { label: I18n.t('fields.key'), key: 'key', type: 'string' }
        ]}
                 update={this.updateFilters}
        />
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('fields.key') }
                >
                  { I18n.t('fields.key') }
                </SortingTh>
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              content_keys.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{item.key}</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'right'}}>
                    <IconButton onTouchTap={() => location.hash = `#/content_key/${item.id}`}>
                      </IconButton>
                      <IconButton onTouchTap={() => location.hash = `#/content_key/${item.id}/edit`}>
                        <ImageEdit color={palette.accent1Color} />
                      </IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this, item) }><ActionDelete
                        color="#c62828"/>
                    </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>

        </Table>
        <Clearfix/>
        <Dialog
        title={ I18n.t('forms.are_you_sure') }
        actions={[
          <FlatButton
            onTouchTap={this.closeConfirm}
            label={ I18n.t('actions.cancel') }
          />,
          <FlatButton
            secondary={true}
            onTouchTap={this.handleDelete}
            label={ I18n.t('actions.confirm') }
          />
        ]}
        modal={false}
        open={showConfirm}
        onRequestClose={this.closeConfirm}
      >
        { I18n.t('forms.you_are_going_to_remove') }
        </Dialog>
      </Paper>
    )
  }
}

ContentKeys.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(ContentKeys)