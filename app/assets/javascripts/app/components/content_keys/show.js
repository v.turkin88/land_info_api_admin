import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ControlLabel, Row, Col} 
from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
} from 'material-ui';

import {paperStyle} from '../common/styles';
import {show, upsert} from '../../services/content_keys';

class ContentKey extends Component {
  state = {
    content_key: {
      content_key_type: '',
      key: '',
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveContentKey();
  }

  _retrieveContentKey = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        content_key: res.content_key,
      })
    });
  };

  render() {
    const {content_key} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('content_key.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/content_keys' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.key') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {content_key.key || '-'}</span>
          </Col>
        </Row>
      </Paper>
    )
  }
}

export default connect(state => state)(ContentKey)
