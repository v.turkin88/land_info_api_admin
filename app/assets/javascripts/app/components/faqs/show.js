import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormGroup, ControlLabel, Row, Col, Clearfix } from 'react-bootstrap';
import { Tabs, Tab } from 'material-ui/Tabs';
import {
  Paper,
  RaisedButton,
  Toggle
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show } from '../../services/faq';
import FroalaEditor from 'react-froala-wysiwyg';

class Faq extends Component {
  state = {
    faq: {
      active: false,
      translates: [
        {
          description: '',
          title: '',
          locale: "uk"
        },
        {
          description: '',
          title: '',
          locale: "en"
        },
      ]
    },
    tabValue: 'uk',
  };

  _handleChangeTab = (value) => {
    this.setState({
      tabValue: value,
    });
  };


  componentWillMount() {
    this._retrieveFaq();
  }

  _retrieveFaq = () => {
    const { id } = this.props.params;
    show(id).success(res => {
      this.setState({
        faq: res.faq
      })
    })
  };

  render() {
    const { faq, tabValue } = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        {console.log(this.state)}
        <Row>
          <Col sm={6}>
            <h2>&nbsp;{I18n.t('faq.show')}</h2>
          </Col>
          <Col sm={6}>
            <RaisedButton
              href='#/faqs'
              className='pull-right'
              secondary={true}
              label={I18n.t('actions.back')}
            />
          </Col>
        </Row>
        <br />
        <FormGroup>
          <Row>
            <Col sm={12}>
              <Toggle
                label={I18n.t('fields.active')}
                toggled={faq.active}
              />
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <Row>
              <Col sm={4}>
                <ControlLabel>{ I18n.t('fields.created_at') }</ControlLabel>
              </Col>
              <Col sm={8}>
                <span className="form-control-static">
                  { faq.created_at }
                </span>
              </Col>
              </Row>
            </Col>
            <Col sm={6}>
              <Row>
              <Col sm={4}>
                <ControlLabel>{ I18n.t('fields.updated_at') }</ControlLabel>
              </Col>
              <Col sm={8}>
                <span className="form-control-static">
                  { faq.updated_at }
                </span>
              </Col>
              </Row>
            </Col>
              
            </Row>
            <hr/>
          <Row>
            <Col sm={12}>
              <Tabs value={tabValue} onChange={this._handleChangeTab}>
                {
                  faq.translates.map((item, index) => {
                    return (
                      <Tab label={item.locale} value={item.locale} key={item.locale}>
                        <Col sm={10}>
                          <br />
                          <Row>
                            <Col sm={2}>
                              <ControlLabel>{I18n.t('fields.title')}</ControlLabel>
                            </Col>
                            <Col sm={10}>
                              <span className="form-control-static">
                                {item.title}
                              </span>
                            </Col>
                          </Row>
                          <Row>
                            <Col sm={2}>
                              <ControlLabel>{I18n.t('fields.description')}</ControlLabel>
                            </Col>
                            <Col sm={10}>
                              <div dangerouslySetInnerHTML={{__html: item.description}}></div>
                            </Col>
                          </Row>
                        </Col>
                      </Tab>
                    )
                  })
                }
              </Tabs>
            </Col>
          </Row>

          <br />
          <hr />
        </FormGroup>
      </Paper>
    )
  }
}

export default connect(state => state)(Faq)
