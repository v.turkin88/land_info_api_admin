import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  CircularProgress,
} from 'material-ui';
import {
  ActionVisibility,
  ImageEdit,
  ActionDelete
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import SortingTh from '../common/sorting_th';
import Filters from '../common/filters_component';
import {paperStyle} from '../common/styles';
import {all, destroy} from '../../services/faq';

class Faqs extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    faqs: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveFaqs();
  }

  _retrieveFaqs = () => {
    const {filters} = this.state;
    all(filters).success(res => {
      this.setState({
        faqs: res.faqs,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveFaqs);
  };

  handleShowSizeChange = (_, per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveFaqs);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveFaqs)
  };

  closeConfirm = () => {
    this.setState({showConfirm: false})
  };

  handleDelete = () => {
    const {selectedRecord} = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveFaqs();
      this.closeConfirm();
    });
  };

  render() {
    const {isLoading} = this.props.app.main;
    const {faqs, showConfirm, count} = this.state;
    const {page, per_page} = this.state.filters;
    const {palette} = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <h2>{ I18n.t('headers.faq') }</h2>
        <Row>
          <Col sm={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10', '20', '50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col sm={4} className="text-right" style={{minHeight: 61}}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36}/>
            <RaisedButton href='#/faq/new' className='pull-right'
                          primary={true} label={ I18n.t('actions.new') }
            />
          </Col>
        </Row>
        <Filters columns={[{ label: I18n.t('fields.active'), key: 'locale_id', type: 'boolean' },
                          ]} update={this.updateFilters}/>
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
            <TableHeaderColumn style={{textAlign: 'left', width: '125px'}}></TableHeaderColumn>
              <TableHeaderColumn style={{textAlign: 'left'}}>{ I18n.t('fields.title') }</TableHeaderColumn>
              <TableHeaderColumn style={{textAlign: 'left'}}>{ I18n.t('fields.locale') }</TableHeaderColumn>
              <TableHeaderColumn style={{textAlign: 'left'}}><SortingTh update={this.updateFilters} column='active'>{ I18n.t('fields.active') }</SortingTh></TableHeaderColumn>
              <TableHeaderColumn style={{textAlign: 'left'}}><SortingTh update={this.updateFilters} column='created_at'>{ I18n.t('fields.created_at') }</SortingTh></TableHeaderColumn>
             
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              faqs.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn style={{textAlign: 'left', width: '125px'}} className='text-right'>
                      <IconButton style={{width: '25px'}}
                        onTouchTap={() => location.hash = `#/faq/${item.id}`}><ActionVisibility
                        color={palette.primary1Color}/></IconButton>
                      <IconButton style={{width: '25px'}}
                        onTouchTap={() => location.hash = `#/faq/${item.id}/edit`}><ImageEdit
                        color={palette.accent1Color}/></IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this, item) } style={{width: '25px'}}><ActionDelete
                        color="#c62828"/></IconButton>
                    </TableRowColumn>
                    <TableRowColumn style={{textAlign: 'left'}}>{item.translates[0].title}</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'left'}}>{item.translates[0].locale}</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'left'}}>{ item.active ? <span style={{color: "#009900"}}>Active</span> : <span style={{color: "#c62828"}}>No</span> }</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'left'}}>{item.created_at}</TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Dialog
          title={ I18n.t('forms.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirm}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handleDelete}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('forms.you_are_going_to_remove') }
        </Dialog>
        <Clearfix/>
      </Paper>
    )
  }
}

Faqs.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Faqs)
