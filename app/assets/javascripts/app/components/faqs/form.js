import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, FormGroup, Clearfix } from "react-bootstrap";
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  Toggle,
} from "material-ui";
import { Tabs, Tab } from "material-ui/Tabs";
import { paperStyle } from "../common/styles";
import { show, upsert } from "../../services/faq";
import { FormErrorMessage } from "../common/form-error-message.component";
import FroalaEditor from 'react-froala-wysiwyg';

class FaqForm extends Component {
  state = {
    faq: {
      active: false,
    },
    translates: [
      {
        description: "",
        title: "",
        locale: "uk",
      },
      {
        description: "",
        title: "",
        locale: "en",
      },
    ],
    tabValue: "uk",
    active: false,
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveFaqs();
  }

  _retrieveFaqs = () => {
    const { id } = this.props.params;
    if (!id) {
      return false;
    }
    show(id).success((res) => {
      res.faq.created_at = new Date(res.faq.created_at);
      res.faq.updated_at = new Date(res.faq.updated_at);
      this.setState({
        faq: res.faq,
        translates: res.faq.translates,
      });
    });
  };

  _handleChangeTab = (value) => {
    this.setState({
      tabValue: value,
    });
  };

  _handleChange = (key, value) => {
    console.log(key, value);
    const { faq } = this.state;

    this.setState(
      {
        faq: {
          ...faq,
          [key]: value,
        },
        validationErrors: {
          ...this.state.validationErrors,
          [key]: null,
        },
      },
      () => {
        // after state update
      }
    );
  };

  _handleChangeTranslates = (index, key, value) => {
    console.log(index, key, value);

    let translates = [...this.state.translates];
    let item = { ...translates[index] };
    item[key] = value;

    translates[index] = item;
    this.setState({ translates });
  };

  _handleSubmit = (event) => {
    event.preventDefault();
    const { faq, translates } = this.state;
    upsert(faq, translates)
      .success((res) => {
        location.hash = "#/faqs";
      })
      .progress((value) => {
        this.setState({ progress: value });
      })
      .error((res) => {
        this.setState({
          validationErrors: res.validation_errors,
        });
      });
  };

  modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" },
      ],
      ["link", "image"],
      ["clean"],
    ],
  };

  formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
  ];

  render() {
    const { isLoading } = this.props.app.main;
    const { faq, translates, tabValue, progress, validationErrors, locales } =
      this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <h2>{faq.id ? I18n.t("faq.edit") : I18n.t("faq.new")}</h2>
          </Col>
          <Col sm={6}>
            <RaisedButton
              href="#/faqs"
              className="pull-right"
              secondary={true}
              label={I18n.t("actions.back")}
            />
          </Col>
        </Row>
        <br />
        <form onSubmit={this._handleSubmit}>
          <FormGroup>
            <Toggle
              label={I18n.t("fields.active")}
              toggled={faq.active}
              onToggle={() => this._handleChange("active", !faq.active)}
            />
          </FormGroup>
          <FormGroup>
            <Tabs value={tabValue} onChange={this._handleChangeTab}>
              {translates.map((item, index) => {
                return (
                  <Tab
                    label={item.locale}
                    value={item.locale}
                    key={item.locale}
                  >
                    <FormGroup>
                      <TextField
                        floatingLabelText={I18n.t("fields.title")}
                        fullWidth={true}
                        value={item.title}
                        onChange={(_, val) =>
                          this._handleChangeTranslates(index, "title", val)
                        }
                        errorText={(validationErrors.title || []).join(", ")}
                      />
                    </FormGroup>

                    <FormGroup>
                      <FroalaEditor
                        tag="div"
                        model={item.description}
                        onModelChange={val => this._handleChangeTranslates(index, 'description', val)}
                        config={{ imageUploadURL: '/admin/attachments/Term' }}
                      />
                    </FormGroup>
                  </Tab>
                );
              })}
            </Tabs>
          </FormGroup>

          <Col sm={4} smOffset={8} className="text-right">
            <br />
            <CircularProgress
              className={
                isLoading && progress > 0 ? "loading-spinner" : "hidden"
              }
              mode="determinate"
              value={progress}
              size={36}
            />
            <RaisedButton
              type="submit"
              primary={true}
              className="pull-right"
              label={I18n.t("actions.submit")}
              disabled={isLoading}
            />
          </Col>
          <Clearfix />
        </form>
      </Paper>
    );
  }
}

export default connect((state) => state)(FaqForm);
