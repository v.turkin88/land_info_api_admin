import React, { Component } from "react";
import { Grid, Row, Col } from 'react-bootstrap';
import {AppBar, Drawer, MenuItem, IconButton, Popover, Menu} from 'material-ui';
import { logout } from '../../services/sessions';
import {
  LogoutIcon, DashboardIcon, GanreIcon, PrivacyPolicyIcon, NewsIcon, AboutIcon, FaqIcon, UsersIcon, FeedbackIcon
} from '../common/icons';
import { SocialGroup, ContentContentPaste, NavigationArrowDropRight, ActionSettings } from 'material-ui/svg-icons'
import { store } from '../../create_store';
const { dispatch } = store;
import { connect } from 'react-redux';
import Feedbacks from "../feedbacks";

class HomePage extends Component {
  state = { open: false };

  handleToggle = () => this.setState({open: true});

  _selectLanguage = (language) => {
    this.setState({
      languagePopoverOpen: false
    })
    dispatch({type: 'SET_LANGUAGE', language});
  }

  render() {
    return (
        <div className="dashboard-page">
          <AppBar
              title="LandCare"
              onLeftIconButtonTouchTap={ this.handleToggle }
          >
            <IconButton
                style={{marginTop: '10px'}}
                onTouchTap={(event => this.setState({languagePopoverOpen: true, anchorEl: event.currentTarget})) }
            >
              <div className={`flag ${ this.props.app.main.language }`}></div>
            </IconButton>
            <Popover
                open={this.state.languagePopoverOpen}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                onRequestClose={() => this.setState({languagePopoverOpen: false}) }
            >
              <Menu>
                <MenuItem onTouchTap={() => this._selectLanguage('ua') }>
                  <div className="flag ua"></div>
                  <span style={{marginLeft: '10px'}}>UKR</span>
                </MenuItem>
                <MenuItem onTouchTap={() => this._selectLanguage('en') }>
                  <div className="flag en"></div>
                  <span style={{marginLeft: '10px'}}>ENG</span>
                </MenuItem>
              </Menu>
            </Popover>

            <IconButton
              style={{marginTop: '10px'}}
              onTouchTap={logout}
            >
              <LogoutIcon/>
            </IconButton>
          </AppBar>
          <Drawer
            docked={false}
            width={300}
            open={this.state.open}
            onRequestChange={(open) => this.setState({open}) }
          >
            <MenuItem onTouchTap={this.handleToggle} href='#/' leftIcon={<DashboardIcon />}>
              { I18n.t('headers.dashboard') }
            </MenuItem>
            {/* <MenuItem onTouchTap={this.handleToggle} href='#/countries'>{ I18n.t('country.header') }</MenuItem> */}
            <MenuItem onTouchTap={this.handleToggle} leftIcon={<FeedbackIcon />} href='#/feedbacks'>{ I18n.t('headers.feedback') }</MenuItem>
            <MenuItem
              primaryText={ I18n.t('headers.users') }
              rightIcon={<NavigationArrowDropRight/>}
              leftIcon={<UsersIcon />}
              menuItems={[
                <MenuItem onTouchTap={this.handleToggle} href='#/users/admin'>{ I18n.t('headers.admin') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/users/manager'>{ I18n.t('headers.manager') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/users/member'>{ I18n.t('headers.member') }</MenuItem>
              ]}
            />
                <MenuItem onTouchTap={this.handleToggle} leftIcon={<FaqIcon />} href='#/faqs'>{ I18n.t('headers.faq') }</MenuItem>
            {/* <MenuItem
              primaryText={ I18n.t('headers.locale') }
              rightIcon={<NavigationArrowDropRight/>}
              menuItems={[ */}
                {/* <MenuItem onTouchTap={this.handleToggle} href='#/locales'>{ I18n.t('headers.locale') }</MenuItem> */}
              {/* ]} */}
            {/* /> */}
            {/* <MenuItem
              primaryText={ I18n.t('headers.translation') }
              rightIcon={<NavigationArrowDropRight/>}
              menuItems={[
                <MenuItem onTouchTap={this.handleToggle} href='#/translations'>{ I18n.t('headers.translation') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/locales'>{ I18n.t('headers.locale') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/content_keys'>{ I18n.t('headers.content_key') }</MenuItem>
              ]}
            /> */}
            <MenuItem onTouchTap={this.handleToggle} leftIcon={<PrivacyPolicyIcon />} href='#/privacy_policies'>{ I18n.t('headers.privacy_policy') }</MenuItem>
            <MenuItem onTouchTap={this.handleToggle} leftIcon={<NewsIcon />} href='#/posts'>{ I18n.t('headers.post') }</MenuItem>
            <MenuItem onTouchTap={this.handleToggle} leftIcon={<AboutIcon />} href='#/abouts'>{ I18n.t('headers.about') }</MenuItem>
            <MenuItem
              primaryText={ I18n.t('headers.system_settings') }
              leftIcon={<ActionSettings/>}
              rightIcon={<NavigationArrowDropRight/>}
              menuItems={[
                <MenuItem onTouchTap={this.handleToggle} href='#/email_sender'>{ I18n.t('headers.email_settings') }</MenuItem>
              ]}
            />
          </Drawer>
          <Grid>
            <Row>
              <Col md={12}>
                <br/>
                {this.props.children}
              </Col>
            </Row>
          </Grid>
        </div>
    );
  }
}

export default connect(state => state)(HomePage);
