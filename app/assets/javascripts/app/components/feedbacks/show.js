import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormGroup, ControlLabel, Row, Col, Clearfix } from 'react-bootstrap';
import { Card, CardText } from 'material-ui/Card';
import { Paper, RaisedButton, FlatButton, TextField } from 'material-ui';
import {paperStyle} from '../common/styles';
import {show,upsert} from '../../services/feedbacks';

class Feedback extends Component {
  state = {
    feedback: {
      id: '',
      email: '',
      full_name: '',
      message: '',
      responce: {},
    },
    expanded: false
  };

  componentWillMount() {
    this._retrieveFeedback();
  }

  _handleExpandChange = (expanded) => {
    this.setState({expanded: expanded});
  };

  _handleExpand = () => {
    this.setState({expanded: true});
  };

  _handleReduce = () => {
    this.setState({expanded: false});
  };

  _handleSubmit = event => {
    event.preventDefault();
    const {feedback} = this.state;
    upsert(feedback)
      .success(res => {
        location.hash = '#/feedbacks';
      })
  };

  _handleChange = (key,value) => {
    const { feedback } = this.state;

    this.setState({
      feedback: {
        ...feedback,
        [key]: value
      }
    }, () => {
      // after state update
    })
  };


  _retrieveFeedback = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        feedback: res.feedback
      })
    })
  };

  render() {
    const {feedback} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <h2>&nbsp;{ I18n.t('feedback.show') }</h2>
          </Col>
          <Col sm={6}>
            <RaisedButton
              href='#/feedbacks'
              className='pull-right'
              secondary={true}
              label={ I18n.t('actions.back') }
            />
          </Col>
        </Row>
        <br/>
        <FormGroup>
          <Row>
            <Col sm={2}>
              <ControlLabel>{ I18n.t('fields.email') }</ControlLabel>
            </Col>
            <Col sm={10}>
                <span className="form-control-static">
                  {feedback.email}
                </span>
            </Col>
          </Row>
        </FormGroup>
        <hr/>
        <FormGroup>
          <Row>
            <Col sm={2}>
              <ControlLabel>{ I18n.t('fields.name') }</ControlLabel>
            </Col>
            <Col sm={10}>
                <span className="form-control-static">
                  {feedback.full_name}
                </span>
            </Col>
          </Row>
        </FormGroup>
        <hr/>
        <FormGroup>
          <Row>
            <Col sm={2}>
              <ControlLabel>{ I18n.t('fields.created_at') }</ControlLabel>
            </Col>
            <Col sm={10}>
                <span className="form-control-static">
                  {feedback.created_at}
                </span>
            </Col>
          </Row>
        </FormGroup>
        <hr/>
        <FormGroup>
          <Row>
            <Col sm={2}>
              <ControlLabel>{ I18n.t('fields.content') }</ControlLabel>
            </Col>
            <Col sm={10}>
                <span className="form-control-static">
                  {feedback.message}
                </span>
            </Col>
          </Row>
          <Row>
            <FlatButton label="Send Responce" className='pull-right' onTouchTap={this._handleExpand}/>
          </Row>
        </FormGroup>

        <FormGroup>
          <Card expanded={this.state.expanded} onExpandChange={this._handleExpandChange}>
            <CardText expandable={true}>
              <form onSubmit={this._handleSubmit}>
                <FormGroup>
                  <Row>
                    <Col sm={12}>
                      <Col sm={6}>
                        <Row>
                          <Col sm={3}>
                            <ControlLabel>{ I18n.t('fields.responce') }</ControlLabel>
                          </Col>
                          <Col sm={9}>
                            <TextField hintText={ I18n.t('fields.responce') }
                                       fullWidth={true}
                                       value={feedback.responce}
                                       onChange={(_, val) => this._handleChange('responce', val) }/>
                          </Col>
                        </Row>
                      </Col>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col sm={4} smOffset={8} className="text-right">
                      <FlatButton label="Cancel" onTouchTap={this._handleReduce}/>
                      <RaisedButton type='submit' primary={true} className='pull-right' label="Send"/>
                    </Col>
                  </Row>
                </FormGroup>
              </form>

            </CardText>
          </Card>
        </FormGroup>
      </Paper>
    )
  }
}

export default connect(state => state)(Feedback)