import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  FlatButton,
  Dialog,
  IconButton,
  Paper
} from 'material-ui';
import {
  ActionVisibility,
  ActionDelete
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import SortingTh from '../common/sorting_th';
import Filters from '../common/filters_component';
import {paperStyle} from '../common/styles';
import {all, destroy} from '../../services/feedbacks';

class Feedbacks extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    feedbacks: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveFeedbacks();
  }

  _retrieveFeedbacks = () => {
    const {filters} = this.state;
    all(filters).success(res => {
      this.setState({
        feedbacks: res.feedbacks,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveFeedbacks);
  };

  handleShowSizeChange = (_, per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveFeedbacks);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveFeedbacks)
  };

  closeConfirm = () => {
    this.setState({showConfirm: false})
  };

  handleDelete = () => {
    const {selectedRecord} = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveFeedbacks();
      this.closeConfirm();
    });
  };

  render() {
    const {isLoading} = this.props.app.main;
    const {feedbacks, showConfirm, count} = this.state;
    const {page, per_page} = this.state.filters;
    const {palette} = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <h2>{ I18n.t('headers.feedback') }</h2>
        <Row>
          <Col sm={12}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10', '20', '50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
        </Row>
        <Filters columns={[{label: 'Email', key: 'email', type: 'string'}, {label: 'Name', key: 'name', type: 'string'},
                ]}
                 update={this.updateFilters}/>
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters} column='email'>{ I18n.t('fields.email') }</SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters} column='name'>{ I18n.t('fields.name') }</SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters}
                           column='created_at'>{ I18n.t('fields.created_at') }</SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters}column='viewed'>{ I18n.t('fields.viewed') }</SortingTh>
              </TableHeaderColumn>

            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              feedbacks.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{item.email}</TableRowColumn>
                    <TableRowColumn>{item.full_name}</TableRowColumn>
                    <TableRowColumn>{item.message}</TableRowColumn>
                    <TableRowColumn className='text-right'>

                      <IconButton
                        onTouchTap={() => location.hash = `#/feedback/${item.id}`}><ActionVisibility
                        color={palette.primary1Color}/>
                      </IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this, item) }><ActionDelete
                        color="#c62828"/>
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Dialog
          title={ I18n.t('forms.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirm}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handleDelete}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('forms.you_are_going_to_remove') }
        </Dialog>
        <Clearfix/>
      </Paper>
    )
  }
}

Feedbacks.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Feedbacks)
