import React, { Component } from 'react';
import { Paper, Chip, Card, CardActions, CardHeader,
         CardMedia, CardTitle, CardText, FlatButton, RadioButton, RadioButtonGroup,
         DatePicker, RaisedButton, Toggle, SelectField, MenuItem
} from 'material-ui';
import { paperStyle } from '../../common/styles';
import { BarChart, Bar, XAxis, YAxis, Tooltip} from 'recharts';
import { all } from '../../../services/piechart_data';
import { Row, Col, ControlLabel } from 'react-bootstrap';

class Blank extends Component {
  state = {
    autoOk: false,
    disableYearSelection: false,
    value: 1,
    from_date: '',
    to_date: '',
    filters: {
      piechart: true,
      value: 1,
      from_date: '',
      to_date: '',
    },
    data: [],
    style: {
      chip: {
        margin: 4,
      },
      wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      charts: {
        margin: 10
      }
    }
  };

  handleChangeFromDate = (event, date) => {
    this.setState({
      from_date: date,
    });
  };

  handleChangeToDate = (event, date) => {
    this.setState({
      to_date: date,
    });
  };

  handleChange = (event, index, value) => this.setState({
    value: value
  });

  componentWillMount() {
    // this._retrieveData();
  };

  _retrieveData = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        data: res.data,
      })
    })

  };
  updateFilters = () => {
    const { filters } = this.state;
    const { value, from_date, to_date } = this.state;
    filters.value = value;
    filters.from_date = from_date;
    filters.to_date = to_date;
    all(filters).success(res => {
      this.setState({
        data: res.data,
      })
    })
  };

  // SimpleLineChart = (dataType) =>{
  //   const {style} = this.state;
  //     return (
  //       <div>
  //         <Col xs={20} style={style.charts}>
  //           <Row >
  //             <Col xs={10}>
  //               <Row>
  //                 <LineChart
  //                   width={950}
  //                   height={300}
  //                   data={dataType}>
  //                   <CartesianGrid strokeDasharray="3 3"/>
  //                   <XAxis
  //                     dataKey="name"
  //                     padding={{left: 30, right: 30}}/>
  //                   <YAxis/>
  //                   <Tooltip/>
  //                   <Legend />
  //                   <Line
  //                     type="monotone"
  //                     dataKey="login"
  //                     stroke="#8884d8"
  //                     activeDot={{r: 8}}/>
  //                   <Line
  //                     type="monotone"
  //                     dataKey="registration"
  //                     stroke="#82ca9d"
  //                     activeDot={{r: 8}}/>
  //                   <Line
  //                     type="monotone"
  //                     dataKey="advertisement"
  //                     stroke="#3786c6"
  //                     activeDot={{r: 8}}/>
  //                 </LineChart>
  //               </Row>
  //             </Col>
  //           </Row>
  //         </Col>
  //       </div>
  //     )
  // };

 

  render() {
    const { filters, chipData, style, data} = this.state;

    const userData = [{name: "Admin",  count: data.admin, color: "#ff6262"},
      {name: "Artist", count: data.artist,  color: "#FF8042"},
      {name: "Business", count: data.business,  color: "#3aff27"},
      {name: "Producer", count: data.producer,  color: "#a8141a"},
      {name: "Regular", count: data.regular,  color: "#2069ff"}
    ];


    return (
      <Paper style={paperStyle} zDepth={1}>
        <Card style={{marginBottom: 10}}>
           <CardHeader
             title={ I18n.t('headers.filters') }
             actAsExpander={true}
             showExpandableButton={true}
           />
           <CardText expandable={true}>
             <Row>
                 <Col sm={4}>
                   <SelectField
                     floatingLabelText="Select Period"
                     value={this.state.value}
                     onChange={this.handleChange}
                   >
                     <MenuItem value={1} primaryText="Default" />
                     <MenuItem value={2} primaryText="Week" />
                     <MenuItem value={3} primaryText="Mounth" />
                     <MenuItem value={4} primaryText="Period" />
                   </SelectField>
                   <br />
                 </Col>
                 <Col sm={3}>
                   <DatePicker
                     onChange={this.handleChangeFromDate}
                     floatingLabelText="From Date"
                     autoOk={this.state.autoOk}
                     disableYearSelection={this.state.disableYearSelection}
                     />
                 </Col>
                 <Col sm={3}>
                   <DatePicker
                     onChange={this.handleChangeToDate}
                     floatingLabelText="To Date"
                     autoOk={this.state.autoOk}
                     disableYearSelection={this.state.disableYearSelection}
                   />
                 </Col>
                 <Col sm={2} className="text-right" style={{minHeight:61}}>
                     <RaisedButton
                       onTouchTap={this.updateFilters}
                       className='pull-right'
                       primary={true}
                       label='Show'
                    />
                </Col>
              </Row>
          </CardText>
        </Card>
        <Card>
          <CardText>
            <div style={style.wrapper}>
              Hello
            </div>
          </CardText>
        </Card>

      </Paper>


    );
  }

}

export default Blank;
