import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, FormGroup, Clearfix } from "react-bootstrap";
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  Toggle, SelectField, MenuItem
} from "material-ui";
import { Tabs, Tab } from "material-ui/Tabs";
import { paperStyle } from "../common/styles";
import { show, upsert } from "../../services/posts";
import { FormErrorMessage } from "../common/form-error-message.component";
import FroalaEditor from 'react-froala-wysiwyg';

class PostForm extends Component {
  state = {
    post: {
      active: false,
      status: 'draft',
    },
    translates: [
      {
        description: "",
        title: "",
        locale: "uk",
      },
      {
        description: "",
        title: "",
        locale: "en",
      },
    ],
    tabValue: "uk",
    active: false,
    validationErrors: {},
  };

  componentWillMount() {
    this._retrievePosts();
  }

  _retrievePosts = () => {
    const { id } = this.props.params;
    if (!id) {
      return false;
    }
    show(id).success((res) => {
      res.post.created_at = new Date(res.post.created_at);
      res.post.updated_at = new Date(res.post.updated_at);
      this.setState({
        post: res.post,
        translates: res.post.translates,
      });
    });
  };

  _handleChangeTab = (value) => {
    this.setState({
      tabValue: value,
    });
  };

  _handleChange = (key, value) => {
    const { post } = this.state;

    this.setState(
      {
        post: {
          ...post,
          [key]: value,
        },
        validationErrors: {
          ...this.state.validationErrors,
          [key]: null,
        },
      },
      () => {
        // after state update
      }
    );
  };

  _handleChangeTranslates = (index, key, value) => {
    console.log(index, key, value);

    let translates = [...this.state.translates];
    let item = { ...translates[index] };
    item[key] = value;

    translates[index] = item;
    this.setState({ translates });
  };

  _handleSubmit = (event) => {
    event.preventDefault();
    const { post, translates } = this.state;
    upsert(post, translates)
      .success((res) => {
        location.hash = "#/posts";
      })
      .progress((value) => {
        this.setState({ progress: value });
      })
      .error((res) => {
        this.setState({
          validationErrors: res.validation_errors,
        });
      });
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { post, translates, tabValue, progress, validationErrors, locales } =
      this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <h2>{post.id ? I18n.t("post.edit") : I18n.t("post.new")}</h2>
          </Col>
          <Col sm={6}>
            <RaisedButton
              href="#/posts"
              className="pull-right"
              secondary={true}
              label={I18n.t("actions.back")}
            />
          </Col>
        </Row>
        <br />
        <form onSubmit={this._handleSubmit}>
          <FormGroup>
            <Toggle
              label={I18n.t("fields.active")}
              toggled={post.active}
              onToggle={() => this._handleChange("active", !post.active)}
            />
          </FormGroup>
          <FormGroup>          
            <SelectField
              floatingLabelText={I18n.t("fields.select_status")}
              value={post.status}
              onChange={(_, __, val) => this._handleChange('status', val)}>
              <MenuItem value={'draft'} primaryText={I18n.t("fields.statuses.draft")} />
              <MenuItem value={'published'} primaryText={I18n.t("fields.statuses.published")} />
              <MenuItem value={'archive'} primaryText={I18n.t("fields.statuses.archive")} />
            </SelectField>
          </FormGroup>
          <FormGroup>
            <Tabs value={tabValue} onChange={this._handleChangeTab}>
              {translates.map((item, index) => {
                return (
                  <Tab
                    label={item.locale}
                    value={item.locale}
                    key={item.locale}
                  >
                    <FormGroup>
                      <TextField
                        floatingLabelText={I18n.t("fields.title")}
                        fullWidth={true}
                        value={item.title}
                        onChange={(_, val) =>
                          this._handleChangeTranslates(index, "title", val)
                        }
                        errorText={(validationErrors.title || []).join(", ")}
                      />
                    </FormGroup>

                    <FormGroup>
                      <FroalaEditor
                        tag="div"
                        model={item.description}
                        onModelChange={val => this._handleChangeTranslates(index, 'description', val)}
                        config={{ imageUploadURL: '/admin/attachments/Term' }}
                      />
                    </FormGroup>
                  </Tab>
                );
              })}
            </Tabs>
          </FormGroup>

          <Col sm={4} smOffset={8} className="text-right">
            <br />
            <CircularProgress
              className={
                isLoading && progress > 0 ? "loading-spinner" : "hidden"
              }
              mode="determinate"
              value={progress}
              size={36}
            />
            <RaisedButton
              type="submit"
              primary={true}
              className="pull-right"
              label={I18n.t("actions.submit")}
              disabled={isLoading}
            />
          </Col>
          <Clearfix />
        </form>
      </Paper>
    );
  }
}

export default connect((state) => state)(PostForm);
