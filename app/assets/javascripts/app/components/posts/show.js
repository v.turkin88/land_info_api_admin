import React, { Component } from "react";
import { connect } from "react-redux";
import { FormGroup, ControlLabel, Row, Col, Clearfix } from "react-bootstrap";
import { Tabs, Tab } from "material-ui/Tabs";
import { Paper, RaisedButton, Toggle, SelectField, MenuItem } from "material-ui";
import { paperStyle } from "../common/styles";
import { show } from "../../services/posts";
import FroalaEditor from "react-froala-wysiwyg";

class Post extends Component {
  state = {
    post: {
      active: false,
      status: 'draft',
      is_admin: false,
      user: {},
      translates: [
        {
          description: "",
          title: "",
          locale: "uk",
        },
        {
          description: "",
          title: "",
          locale: "en",
        },
      ],
    },
    tabValue: "uk",
  };

  _handleChangeTab = (value) => {
    this.setState({
      tabValue: value,
    });
  };

  componentWillMount() {
    this._retrievePost();
  }

  _retrievePost = () => {
    const { id } = this.props.params;
    show(id).success((res) => {
      this.setState({
        post: res.post,
      });
    });
  };

  render() {
    const { post, tabValue } = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        {console.log(this.state)}
        <Row>
          <Col sm={6}>
            <h2>&nbsp;{I18n.t("post.show")}</h2>
          </Col>
          <Col sm={6}>
            <RaisedButton
              href="#/posts"
              className="pull-right"
              secondary={true}
              label={I18n.t("actions.back")}
            />
          </Col>
        </Row>
        <br />
        <FormGroup>
          <Row>
            <Col sm={12}>
              <Toggle label={I18n.t("fields.active")} toggled={post.active} />
              <Toggle label={I18n.t("fields.is_admin")} toggled={post.is_admin} />
              <SelectField
              floatingLabelText={I18n.t("fields.select_status")}
              value={post.status}
              disabled={true}
              onChange={(_, __, val) => this._handleChange('status', val)}>
              <MenuItem value={'draft'} primaryText={I18n.t("fields.statuses.draft")} />
              <MenuItem value={'published'} primaryText={I18n.t("fields.statuses.published")} />
              <MenuItem value={'archive'} primaryText={I18n.t("fields.statuses.archive")} />
            </SelectField>
            <Row>
                <Col sm={4}>
                  <ControlLabel>{I18n.t("fields.created_by")}</ControlLabel>
                </Col>
                <Col sm={8}>
                  <span className="form-control-static">{post.user.first_name} {post.user.last_name}</span>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <Row>
                <Col sm={4}>
                  <ControlLabel>{I18n.t("fields.created_at")}</ControlLabel>
                </Col>
                <Col sm={8}>
                  <span className="form-control-static">{post.created_at}</span>
                </Col>
              </Row>
            </Col>
            <Col sm={6}>
              <Row>
                <Col sm={4}>
                  <ControlLabel>{I18n.t("fields.updated_at")}</ControlLabel>
                </Col>
                <Col sm={8}>
                  <span className="form-control-static">{post.updated_at}</span>
                </Col>
              </Row>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col sm={12}>
              <Tabs value={tabValue} onChange={this._handleChangeTab}>
                {post.translates.map((item, index) => {
                  return (
                    <Tab
                      label={item.locale}
                      value={item.locale}
                      key={item.locale}
                    >
                      <Col sm={10}>
                        <br />
                        <Row>
                          <Col sm={2}>
                            <ControlLabel>
                              {I18n.t("fields.title")}
                            </ControlLabel>
                          </Col>
                          <Col sm={10}>
                            <span className="form-control-static">
                              {item.title}
                            </span>
                          </Col>
                        </Row>
                        <Row>
                          <Col sm={2}>
                            <ControlLabel>
                              {I18n.t("fields.description")}
                            </ControlLabel>
                          </Col>
                          <Col sm={10}>
                            <div
                              dangerouslySetInnerHTML={{
                                __html: item.description,
                              }}
                            ></div>
                          </Col>
                        </Row>
                      </Col>
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>

          <br />
          <hr />
        </FormGroup>
      </Paper>
    );
  }
}

export default connect((state) => state)(Post);
