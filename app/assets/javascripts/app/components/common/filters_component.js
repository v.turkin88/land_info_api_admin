import React, { Component } from 'react';
import {
  TextField,
  RaisedButton,
  Card,
  CardHeader,
  CardText,
  DatePicker,
  Toggle,
  TimePicker,
  SelectField,
  MenuItem
} from 'material-ui';
import { Row, Col } from 'react-bootstrap';

export default class Filters extends Component {
  state = {};

  updateParent() {
    const { update } = this.props;
    const state = Object.keys(this.state).map(key => {
      return {[key]:this.state[key]}
    });
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      update(state);
    }, 500)
  };

  updateState = (key,value) => {
    this.setState({ [key]:value }, this.updateParent)
  };

  clearState = () => {
    let hash = {};
    Object.keys(this.state).forEach(key => hash[key] = '');
    this.setState(hash, this.updateParent)
  };

  renderFilterUnit = (column) => {
    switch (column.type) {
      case 'datetime':
        return <DatePicker hintText={column.label} container="inline"
                           value={this.state[column.key]}
                           onChange={(_,val) => this.updateState(column.key, val) } />;
      case 'boolean':
        return <Toggle label={column.label}
                       toggled={this.state[column.key] || false}
                       onToggle={(_,val) => this.updateState(column.key, val) } />;
      case 'time':
        return <TimePicker hintText={column.label}
                           value={this.state[column.key]}
                           onChange={(_,val) => this.updateState(column.key, val) } />;
      case 'select':
        return (<SelectField
          floatingLabelText={column.label}
          value={this.state[column.key]}
          onChange={(_, __, val) => this.updateState(column.key, val) }
        >
          <MenuItem value={''} primaryText='' />
          {Object.keys(column.data_keys).map((k, index) => {
            return(<MenuItem key={index} value={k} primaryText={column.data_keys[k]}/>)
          }) }
        </SelectField>);
      default:
        return <TextField
          floatingLabelText={column.label}
          value={this.state[column.key] || ''}
          name={column.key}
          onChange={(_,val) => this.updateState(column.key,val) }
        />
    }
  };

  render() {
    const { columns } = this.props;

    return(
      <Card style={{marginBottom: 10}}>
        <CardHeader
          title={ I18n.t('headers.filters') }
          actAsExpander={true}
          showExpandableButton={true}
        />
        <CardText expandable={true}>
          <Row>
            {
              Object.keys(columns).map((item,i) => {
                return (<Col md={3} key={i} style={{display: 'flex', height: '72px', alignItems: 'flex-end'}}>
                    { this.renderFilterUnit(columns[i]) }
                  </Col>
                )
              })
            }
            <Col md={12} style={{textAlign: 'right'}}>
              <RaisedButton label={ I18n.t('actions.reset') } secondary={true} onTouchTap={this.clearState} />
            </Col>
          </Row>
        </CardText>
      </Card>
    )
  }
}
