import LogoutIcon from './logout'
import DashboardIcon from './dashboard';
import EmailSettingsIcon from './email_settings';
import GanreIcon from './ganres';
import SystemSubscribeIcon from './system_subscribers';
import FeedbackIcon from './feedbacks';
import SystemNotificationsIcon from './system_notifications';
import PrivacyPolicyIcon from './pivacy_policy';
import NewsIcon from './news';
import AboutIcon from './about';
import FaqIcon from './faq';
import UsersIcon from './users';

export {
  LogoutIcon,
  DashboardIcon,
  EmailSettingsIcon,
  GanreIcon,
  SystemSubscribeIcon,
  FeedbackIcon,
  SystemNotificationsIcon,
  PrivacyPolicyIcon,
  NewsIcon,
  AboutIcon,
  FaqIcon,
  UsersIcon
}
