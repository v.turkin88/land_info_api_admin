import React from "react";
import SvgIcon from "material-ui/SvgIcon";

const AboutIcon = (props) => (
  <SvgIcon {...props}>
    <svg x="0px" y="0px" viewBox="0 0 24 24">
      <g>
        <path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm-.5,3A1.5,1.5,0,1,1,10,6.5,1.5,1.5,0,0,1,11.5,5ZM14,18H13a2,2,0,0,1-2-2V12a1,1,0,0,1,0-2h1a1,1,0,0,1,1,1v5h1a1,1,0,0,1,0,2Z" fill="#464646"/>
      </g>  
    </svg>
  </SvgIcon>
);

export default AboutIcon;
