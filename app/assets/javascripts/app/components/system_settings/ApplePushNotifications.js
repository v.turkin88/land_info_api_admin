import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Col,
  FormGroup,
  Clearfix
} from 'react-bootstrap';
import {Paper, RaisedButton, TextField, CircularProgress, RadioButton, RadioButtonGroup} from 'material-ui';
import { paperStyle } from '../common/styles';
import FileSelect from '../common/file_select_component';
import { show, update } from '../../services/system_settings';

class ApplePushNotificationsForm extends Component {
  state = {
    system_settings: {
      ios_push_environment: 'sandbox',
      apple_purchase_password: '',
      ios_push_apns_host: '',
      ios_push_password: '',
      ios_push_certificate: {}
    }
  };

  componentWillMount() {
    this._retrieveSystemSettings();
  }

  _retrieveSystemSettings = () => {
    show().success(res => {
      this.setState({
        system_settings: res.system_settings
      });
    })
  };

  handleChange = (key,value) => {
    this.setState({
      system_settings: {
        ...this.state.system_settings,
        [key]: value
      }
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { system_settings } = this.state;
    update(system_settings)
      .success(res => {

      })
      .progress(value => {
        this.setState({ progress: value })
      })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { system_settings, progress } = this.state;

    return (
      <div>

        <Paper style={paperStyle} zDepth={1}>
          <h2>{ I18n.t('system_settings.ios_push_environment') }</h2>
          <br/>
          <form onSubmit={this.handleSubmit}>
            <FormGroup>
              <RadioButtonGroup
                name="ios_push_environment"
                defaultSelected="sandbox"
                valueSelected={system_settings.ios_push_environment}
                value={system_settings.ios_push_environment}
                onChange={(_,val) => this.handleChange('ios_push_environment', val) }
              >
                <RadioButton
                  value="production"
                  label={ I18n.t('system_settings.ios_push_environment_production') }
                />
                <RadioButton
                  value="sandbox"
                  label={ I18n.t('system_settings.ios_push_environment_sandbox') }
                />
              </RadioButtonGroup>
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.ios_push_apns_host') }
                fullWidth={true}
                value={system_settings.ios_push_apns_host || 'gateway.sandbox.push.apple.com'}
                onChange={(_,val) => this.handleChange('ios_push_apns_host', val) }
              />
            </FormGroup>


            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.password') }
                type = 'password'
                hintText=''
                fullWidth={true}
                value={system_settings.ios_push_password || ''}
                onChange={(_,val) => this.handleChange('ios_push_password', val) }
              />
            </FormGroup>
            <FormGroup>
              <FileSelect accept={'.pem'}
                          files={[system_settings.ios_push_certificate]}
                          onChange={(files)=>this.handleChange('ios_push_certificate', files[0]) }
                          errorUpdate={()=>({}) }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.apple_purchase_password') }
                hintText=''
                fullWidth={true}
                value={system_settings.apple_purchase_password || ''}
                onChange={(_,val) => this.handleChange('apple_purchase_password', val) }
              />
            </FormGroup>

            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label="Save" disabled={isLoading} />
            </Col>
            <Clearfix/>
          </form>
        </Paper>
      </div>
    )
  }
}

export default connect(state => state)(ApplePushNotificationsForm)
