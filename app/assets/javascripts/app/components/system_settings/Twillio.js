import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Col,
  FormGroup,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show, update } from '../../services/system_settings';

class TwillioForm extends Component {
  state = {
    system_settings: {
      twilio_account_sid: '',
      twilio_auth_token: '',
      twilio_phone_number: ''
    }
  }

  componentWillMount() {
    this._retrieveSystemSettings();
  }

  _retrieveSystemSettings = () => {
    show().success(res => {
      this.setState({
        system_settings: res.system_settings
      })
    })
  };

  handleChange = (key,value) => {
    this.setState({
      system_settings: {
        ...this.state.system_settings,
        [key]: value
      }
    })
  };

  handleSubmit = event => {
    event.preventDefault();
    const { system_settings } = this.state;
    update(system_settings)
      .success(res => {

      })
      .progress(value => {
        this.setState({ progress: value })
      })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { system_settings, progress } = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <h2>{ I18n.t('system_settings.twillio') }</h2>
        <br/>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('system_settings.twilio_account_sid') }
              hintText=''
              fullWidth={true}
              value={system_settings.twilio_account_sid || ''}
              onChange={(_,val) => this.handleChange('twilio_account_sid', val) }
            />
          </FormGroup>

          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('system_settings.twilio_auth_token') }
              hintText=''
              fullWidth={true}
              value={system_settings.twilio_auth_token || ''}
              onChange={(_,val) => this.handleChange('twilio_auth_token', val) }
            />
          </FormGroup>

          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('system_settings.twilio_phone_number') }
              hintText=''
              fullWidth={true}
              value={system_settings.twilio_phone_number || ''}
              onChange={(_,val) => this.handleChange('twilio_phone_number', val) }
            />
          </FormGroup>

          <Col sm={4} smOffset={8} className="text-right">
            <br/>
            <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
            <RaisedButton type='submit' primary={true} className='pull-right' label="Save" disabled={isLoading} />
          </Col>
          <Clearfix/>
        </form>
      </Paper>
    )
  }
}

export default connect(state => state)(TwillioForm)