import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Col,
  FormGroup,
  Clearfix
} from 'react-bootstrap';
import {Paper, RaisedButton, TextField, CircularProgress, RadioButton, RadioButtonGroup} from 'material-ui';
import { paperStyle } from '../common/styles';
import FileSelect from '../common/file_select_component';
import { show, update } from '../../services/system_settings';

class FileStoragesForm extends Component {
  state = {
    system_settings: {
      s3_bucket: '',
      aws_access_key_id: '',
      aws_secret_access_key: '',
      s3_region: ''
    }
  };

  componentWillMount() {
    this._retrieveSystemSettings();
  }

  _retrieveSystemSettings = () => {
    show().success(res => {
      this.setState({
        system_settings: res.system_settings
      });
    })
  };

  handleChange = (key,value) => {
    this.setState({
      system_settings: {
        ...this.state.system_settings,
        [key]: value
      }
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { system_settings } = this.state;
    update(system_settings)
      .success(res => {

      })
      .progress(value => {
        this.setState({ progress: value })
      })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { system_settings, progress } = this.state;

    return (
      <div>
        <Paper style={paperStyle} zDepth={1}>
          <h2>{ I18n.t('system_settings.aws') }</h2>
          <br/>
          <form onSubmit={this.handleSubmit}>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.s3_bucket') }
                hintText=''
                fullWidth={true}
                value={system_settings.s3_bucket || ''}
                onChange={(_,val) => this.handleChange('s3_bucket', val) }
              />
            </FormGroup>

            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.aws_access_key_id') }
                hintText=''
                fullWidth={true}
                value={system_settings.aws_access_key_id || ''}
                onChange={(_,val) => this.handleChange('aws_access_key_id', val) }
              />
            </FormGroup>

            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.aws_secret_access_key') }
                hintText=''
                fullWidth={true}
                value={system_settings.aws_secret_access_key || ''}
                onChange={(_,val) => this.handleChange('aws_secret_access_key', val) }
              />
            </FormGroup>

            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.s3_region') }
                hintText='eu-central-1'
                fullWidth={true}
                value={system_settings.s3_region || ''}
                onChange={(_,val) => this.handleChange('s3_region', val) }
              />
            </FormGroup>

            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label="Save" disabled={isLoading} />
            </Col>
            <Clearfix/>
          </form>
        </Paper>
      </div>
    )
  }
}

export default connect(state => state)(FileStoragesForm)