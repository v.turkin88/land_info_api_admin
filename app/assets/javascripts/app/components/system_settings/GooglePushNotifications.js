import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Col,
  FormGroup,
  Clearfix
} from 'react-bootstrap';
import {Paper, RaisedButton, TextField, CircularProgress, RadioButton, RadioButtonGroup} from 'material-ui';
import { paperStyle } from '../common/styles';
import FileSelect from '../common/file_select_component';
import { show, update } from '../../services/system_settings';

class GooglePushNotificationsForm extends Component {
  state = {
    system_settings: {
      android_push_token: '',
      android_purchase_package_name: '',
      android_purchase_product_id: '',
      android_purchase_refresh_token: '',
      android_purchase_client_id: '',
      android_purchase_client_secret: '',
      android_purchase_redirect_uri: '',
    }
  };

  componentWillMount() {
    this._retrieveSystemSettings();
  }

  _retrieveSystemSettings = () => {
    show().success(res => {
      this.setState({
        system_settings: res.system_settings
      });
    })
  };

  handleChange = (key,value) => {
    this.setState({
      system_settings: {
        ...this.state.system_settings,
        [key]: value
      }
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { system_settings } = this.state;
    update(system_settings)
      .success(res => {

      })
      .progress(value => {
        this.setState({ progress: value })
      })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { system_settings, progress } = this.state;

    return (
      <div>
        <Paper style={paperStyle} zDepth={1}>
          <h2>{ I18n.t('system_settings.android_purchases') }</h2>
          <br/>
          <form onSubmit={this.handleSubmit}>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_push_token') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_push_token || ''}
                onChange={(_,val) => this.handleChange('android_push_token', val) }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_package_name') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_package_name || ''}
                onChange={(_,val) => this.handleChange('android_purchase_package_name', val) }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_product_id') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_product_id || ''}
                onChange={(_,val) => this.handleChange('android_purchase_product_id', val) }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_refresh_token') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_refresh_token || ''}
                onChange={(_,val) => this.handleChange('android_purchase_refresh_token', val) }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_client_id') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_client_id || ''}
                onChange={(_,val) => this.handleChange('android_purchase_client_id', val) }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_client_secret') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_client_secret || ''}
                onChange={(_,val) => this.handleChange('android_purchase_client_secret', val) }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_redirect_uri') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_redirect_uri || ''}
                onChange={(_,val) => this.handleChange('android_purchase_redirect_uri', val) }
              />
            </FormGroup>

            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label="Save" disabled={isLoading} />
            </Col>
            <Clearfix/>
          </form>
        </Paper>
      </div>
    )
  }
}

export default connect(state => state)(GooglePushNotificationsForm)
