import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ControlLabel, Row, Col, Clearfix} 
from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
} from 'material-ui';

import {paperStyle} from '../common/styles';
import {show, upsert} from '../../services/translations';

class Translation extends Component {
  state = {
    translation: {
      content_key: '',
      locale: '',
      title: '',
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveTranslation();
  }

  _retrieveTranslation = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        translation: res.translation,
      })
    });
  };

  render() {
    const {translation} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('translation.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/translations' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.title') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {translation.title}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.locale') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {translation.locale}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.content_key') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {translation.content_key}</span>
          </Col>
        </Row>
        <hr/>
      </Paper>
    )
  }
}

export default connect(state => state)(Translation)
