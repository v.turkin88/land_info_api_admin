import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  Checkbox,
  CircularProgress
} from 'material-ui';
import SortingTh from '../common/sorting_th';
import {
  ImageEdit,
  ActionDelete,
  ActionVisibility
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import Filters from '../common/filters_component';
import { paperStyle } from '../common/styles';
import {all, activate, destroy} from '../../services/translations'

class Translations extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    translations: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveTranslations();
  }
  
  _retrieveTranslations = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        translations: res.translations,
        count: res.count
      });
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveTranslations);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveTranslations);
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveTranslations)
  };

  _changeSelected = (id, isSelected) => {
    let translation = {
      id: id
    };
    activate(translation)
      .success(res => {
        this._retrieveTranslations()
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
        this.setState({
          validationErrors: res.validation_errors
        });
      });
  };

  prepareToAction = (record, action) => {
    this.setState({
      selectedRecord: record,
      showConfirm: true,
      action: action
    })
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  handleDelete = () => {
    const {selectedRecord} = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveTranslations();
      this.closeConfirm();
    });
  };

  handleAction = () => {
    const { selectedRecord, action } = this.state;
    switch (action) {
      case 'destroy':
        destroy(selectedRecord.id).success(res => {
          this._retrieveTranslations();
          this.closeConfirm();
        });
        break;
    }
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { translations, count, showConfirm } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ul className="breadcrumb">
              <li>{ I18n.t('headers.translation') }</li>
            </ul>
          </Col>
        </Row>

        <Row>
          <Col md={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10','20','50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col md={4}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36} />
            <RaisedButton
              href='#/translation/new'
              className='pull-right'
              primary={true}
              label={ I18n.t('actions.new') }
            />
          </Col>
        </Row>

        <br/>
        <Filters columns={[
          { label: I18n.t('fields.locale'), key: 'locale_id', type: 'string' },
          { label: I18n.t('fields.content_key'), key: 'content_key_id', type: 'string' },
          // { label: I18n.t('fields.active'), key: 'active', type: 'boolean'}
          { label: I18n.t('fields.title'), key: 'title', type: 'string'}
        ]}
                 update={this.updateFilters}
        />
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              {/* <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('fields.active') }
                >
                  { I18n.t('fields.active') }
                </SortingTh>
              </TableHeaderColumn> */}
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('fields.title') }
                >
                  { I18n.t('fields.title') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('fields.locale') }
                >
                  { I18n.t('fields.locale') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('fields.content_key') }
                >
                  { I18n.t('fields.content_key') }
                </SortingTh>
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              translations.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{item.title}</TableRowColumn>
                    <TableRowColumn>{item.locale ? item.locale : ''}</TableRowColumn>
                    <TableRowColumn>{item.content_key ? item.content_key : ''}</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'right'}}>
                    <IconButton onTouchTap={() => location.hash = `#/translation/${item.id}`}>
                        <ActionVisibility color={palette.primary1Color}/>
                      </IconButton>
                      <IconButton onTouchTap={() => location.hash = `#/translation/${item.id}/edit`}>
                        <ImageEdit color={palette.accent1Color} />
                      </IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this, item) }><ActionDelete
                        color="#c62828"/>
                    </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Clearfix/>
        <Dialog
        title={ I18n.t('forms.are_you_sure') }
        actions={[
          <FlatButton
            onTouchTap={this.closeConfirm}
            label={ I18n.t('actions.cancel') }
          />,
          <FlatButton
            secondary={true}
            onTouchTap={this.handleDelete}
            label={ I18n.t('actions.confirm') }
          />
        ]}
        modal={false}
        open={showConfirm}
        onRequestClose={this.closeConfirm}
      >
        { I18n.t('forms.you_are_going_to_remove') }
        </Dialog>
      </Paper>
    )
  }
}

Translations.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Translations)