import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix,
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  AutoComplete,
  SelectField,
  Toggle,
  MenuItem
} from 'material-ui';
import {paperStyle} from '../common/styles';
import {show, upsert} from '../../services/translations';
import { FormErrorMessage } from '../common/form-error-message.component'
import { all as getAllLocales }  from '../../services/locales';
import { all as getAllContentKeys }  from '../../services/content_keys';


class TranslationForm extends Component {
  state = {
    translation: {
      content_key: '',
      locale: '',
      title: ''
    },
    content_keys: [],
    locales: [],
    active: false,
    validationErrors: {}
  };

  componentWillMount() {
    this._retrieveTranslation();
    this._retrieveLocaleSearch();
    this._retrieveContentKeySearch();
  }


  _retrieveTranslation = () => {
    const { id } = this.props.params;
    if (!id) { return false }
    show(id).success(res => {
      res.translation.created_at = new Date(res.translation.created_at);
      res.translation.updated_at = new Date(res.translation.updated_at);
      this.setState({
        translation: res.translation
      })
    })
  };

  _handleChange = (key, value) => {
    const {translation} = this.state;
    this.setState({
      translation: {
        ...translation,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };

  handleSubmit = event => {
    event.preventDefault();
    const {translation} = this.state;
    upsert(translation)
      .success(res => {
        location.hash = '#/translations';
      })
      .progress(value => {
        this.setState({progress: value})
      })
  };

  _retrieveContentKeySearch = (value = '') => {
    if (this._contentKeySearchTimeout) {
      clearTimeout(this._contentKeySearchTimeout);
    }
    this._contentKeySearchTimeout = setTimeout( () => {
      getAllContentKeys({page: 1, per_page: 10}).success(res => {
        this.setState({
          content_keys: res.content_keys
        })
      })
    }, 500);
  };

  _retrieveLocaleSearch = (value = '') => {
    if (this._localeSearchTimeout) {
      clearTimeout(this._localeSearchTimeout);
    }
    this._localeSearchTimeout = setTimeout( () => {
      getAllLocales({page: 1, per_page: 10}).success(res => {
        this.setState({
          locales: res.locales
        })
      })
    }, 500);
  };

  render() {
    const {isLoading} = this.props.app.main;
    const {translation, progress, validationErrors, content_keys, locales} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
      <Row>
        <Col sm={6}>
          <h2>{ translation.id ? I18n.t('translation.edit') : I18n.t('translation.new') }</h2>
        </Col>
        <Col sm={6}>
          <RaisedButton
            href='#/translations'
            className='pull-right'
            secondary={true}
            label={ I18n.t('actions.back') }
          />
        </Col>
      </Row>

        <br/>
        <form onSubmit={this.handleSubmit}>

        <FormGroup>
            <AutoComplete
              searchText={ translation.content_key ? translation.content_key.key : ''}
              errorText={ (validationErrors.content_key || []).join('. ') }
              floatingLabelText={ I18n.t('fields.content_key') }
              dataSource={content_keys}
              fullWidth={true}
              dataSourceConfig={{text: 'key', value: 'id'}}
              filter={AutoComplete.caseInsensitiveFilter}
              openOnFocus={true}
              onNewRequest={(val) => this._handleChange('content_key', val) }
              onUpdateInput={(val) => this._retrieveContentKeySearch(val) }
              maxSearchResults={5}
            />
          </FormGroup>

          <FormGroup>
            <AutoComplete
              searchText={ translation.locale ? translation.locale.title : ''}
              errorText={ (validationErrors.locale || []).join('. ') }
              floatingLabelText={ I18n.t('fields.locale') }
              dataSource={locales}
              fullWidth={true}
              dataSourceConfig={{text: 'title', value: 'id'}}
              filter={AutoComplete.caseInsensitiveFilter}
              openOnFocus={true}
              onNewRequest={(val) => this._handleChange('locale', val) }
              onUpdateInput={(val) => this._retrieveLocaleSearch(val) }
              maxSearchResults={5}
            />
          </FormGroup>

        <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('fields.title') }
              fullWidth={true}
              value={translation.title}
              onChange={(_,val) => this._handleChange('title', val) }
              errorText={ (validationErrors.key || []).join(', ') }
            />
        </FormGroup>

        <FormGroup>
              <ControlLabel>Active</ControlLabel>
              <Toggle
                defaultToggled={ translation.active }
                onToggle={ (_, val) => this._handleChange('active', val) }
              />
              <Clearfix/>
              <FormErrorMessage errors={ validationErrors.active  } />
            </FormGroup>

          <Col sm={4} smOffset={8} className="text-right">
            <br/>     
            <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36}/>
            <RaisedButton type='submit'
            primary={true}
            className='pull-right'
            label={ I18n.t('actions.submit') }
            disabled={isLoading}/>
          </Col>
          
          <Clearfix />
        </form>
      </Paper>

    )
  }
}

export default connect(state => state)(TranslationForm)
