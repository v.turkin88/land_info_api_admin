import React, {Component} from 'react';
import {connect} from 'react-redux';
import {FormGroup, ControlLabel, Row, Col, Clearfix } 
from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
} from 'material-ui';

import {paperStyle} from '../common/styles';
import {show, upsert} from '../../services/abouts';
import PropTypes from "prop-types";

class About extends Component {
  state = {
    about: {
      body: '',
      locale: {}
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveAbout();
  }

  _retrieveAbout = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        about: res.about,
      })
    });
  };

  render() {
    const {about} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('about.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/abouts' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>

        <Row>
          <Col lg={ 12 } md={ 12 } sm={ 12 } xs={ 12 }>
            <ControlLabel>{ I18n.t('fields.locale') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {about.locale.title || '-'}</span>
          </Col>
        </Row>
      </Paper>
    )
  }
}

About.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(About)
