import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  CircularProgress,
  TextField
} from 'material-ui';
import {
  ActionVisibility,
  ImageEdit,
  ActionDelete
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import SortingTh from '../common/sorting_th';
import Filters from '../common/filters_component';
import {paperStyle} from '../common/styles';
import {all, destroy} from '../../services/abouts';

class Abouts extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    abouts: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveAbouts();
  }

  _retrieveAbouts = () => {
    const {filters} = this.state;
    all(filters).success(res => {
      this.setState({
        abouts: res.abouts,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveAbouts);
  };

  handleShowSizeChange = (_, per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveAbouts);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveAbouts)
  };

  closeConfirm = () => {
    this.setState({showConfirm: false})
  };

  handleDelete = () => {
    const {selectedRecord} = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveAbouts();
      this.closeConfirm();
    });
  };

  render() {
    const {isLoading} = this.props.app.main;
    const {abouts, showConfirm, count} = this.state;
    const {page, per_page} = this.state.filters;
    const {palette} = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <h2>{ I18n.t('headers.about') }</h2>
        <Row>
          <Col sm={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10', '20', '50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col sm={4} className="text-right" style={{minHeight: 61}}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36}/>
            <RaisedButton href='#/about/new' className='pull-right'
                          primary={true} label={ I18n.t('actions.new') }
            />
          </Col>
        </Row>
        <Filters columns={[{label: 'Body', key: 'body', type: 'body'},
        { label: I18n.t('fields.locale'), key: 'locale_id', type: 'string' },
                          ]} update={this.updateFilters}/>
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>
                <SortingTh 
                  update={this.updateFilters}
                  column='body'>{ I18n.t('fields.body') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('fields.locale') }
                >
                  { I18n.t('fields.locale') }
                </SortingTh>
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              abouts.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{item.body}</TableRowColumn>
                    <TableRowColumn>{item.locale ? item.locale : ''}</TableRowColumn>
                    <TableRowColumn className='text-right'>

                      <IconButton
                        onTouchTap={() => location.hash = `#/about/${item.id}`}><ActionVisibility
                        color={palette.primary1Color}/></IconButton>
                      <IconButton
                        onTouchTap={() => location.hash = `#/about/${item.id}/edit`}><ImageEdit
                        color={palette.accent1Color}/></IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this, item) }><ActionDelete
                        color="#c62828"/></IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Dialog
          title={ I18n.t('forms.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirm}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handleDelete}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('forms.you_are_going_to_remove') }
        </Dialog>
        <Clearfix/>
      </Paper>
    )
  }
}

Abouts.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Abouts)
