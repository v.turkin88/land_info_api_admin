import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  ControlLabel,
  FormGroup,
  Clearfix,
  Button
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  CircularProgress,
  TextField,
  Toggle,
  AutoComplete
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show, upsert } from '../../services/abouts';
import { FormErrorMessage } from '../common/form-error-message.component'
import { all as getAllLocales } from '../../services/locales';

class AboutForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      about: {
        body: '',
        locale: {},
      },
      locales: [],
      validationErrors: {},
    }
    this.handleChange = this.handleChange.bind(this)
  }

  componentWillMount() {
    this._retrieveAbouts();
    this._retrieveLocaleSearch();
  }

  _retrieveAbouts = () => {
    const { id } = this.props.params;
    if (!id) { return false }
    show(id).success(res => {
      res.about.created_at = new Date(res.about.created_at);
      res.about.updated_at = new Date(res.about.updated_at);
      this.setState({
        about: res.about
      })
    })
  };

  _handleChange = (key, value) => {
    const { about } = this.state;
    this.setState({
      about: {
        ...about,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };

  handleChange(value) {
    const { about } = this.state;
    this.setState({
      about: {
        ...about,
        body: value
      }
    })
  }


  _handleSubmit = event => {
    event.preventDefault();
    const { about } = this.state;
    upsert(about)
      .success(res => {
        location.hash = '#/abouts';
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
        this.setState({
          validationErrors: res.validation_errors
        })
      })
  };

  updateFlag = file => {
    this.setState({
      about: {
        ...this.state.about,
        flag: file
      }
    })
  };

  _retrieveLocaleSearch = (value = '') => {
    if (this._localeSearchTimeout) {
      clearTimeout(this._localeSearchTimeout);
    }
    this._localeSearchTimeout = setTimeout(() => {
      getAllLocales({ page: 1, per_page: 10 }).success(res => {
        this.setState({
          locales: res.locales
        })
      })
    }, 500);
  };

  modules = {
    toolbar: [
      [{ 'header': [1, 2, false] }],
      ['bold', 'italic', 'underline','strike', 'blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
      ['link', 'image'],
      ['clean']
    ],
  }

  formats = [
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image'
  ]

  render() {
    const { isLoading } = this.props.app.main;
    const { about, progress, validationErrors, locales } = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <h2>{ about.id ? I18n.t('about.edit') : I18n.t('about.new') }</h2>
          </Col>
          <Col sm={6}>
            <RaisedButton
              href='#/abouts'
              className='pull-right'
              secondary={true}
              label={ I18n.t('actions.back') }
            />
          </Col>
        </Row>

        <br />
        <form onSubmit={this._handleSubmit}>

          <FormGroup>
            <AutoComplete
              searchText={about.locale ? about.locale.title : ''}
              errorText={(validationErrors.locale || []).join('. ') }
              floatingLabelText={ I18n.t('fields.locale') }
              dataSource={locales}
              fullWidth={true}
              dataSourceConfig={{ text: 'title', value: 'id' }}
              filter={AutoComplete.caseInsensitiveFilter}
              openOnFocus={true}
              onNewRequest={(val) => this._handleChange('locale', val) }
              onUpdateInput={(val) => this._retrieveLocaleSearch(val) }
              maxSearchResults={5}
            />
          </FormGroup>

          <Col sm={4} smOffset={8} className="text-right">
            <br />
            <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
            <RaisedButton type='submit'
              primary={true}
              className='pull-right'
              label={ I18n.t('actions.submit') }
              disabled={isLoading}
            />
          </Col>
          <Clearfix />
        </form>
      </Paper>
    )
  }
}

export default connect(state => state)(AboutForm)
