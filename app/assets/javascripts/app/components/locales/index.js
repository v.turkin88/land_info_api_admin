import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  CircularProgress,
} from 'material-ui';
import {
  ActionVisibility,
  ImageEdit,
  ActionDelete
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import SortingTh from '../common/sorting_th';
import Filters from '../common/filters_component';
import {paperStyle} from '../common/styles';
import {all, activate, destroy} from '../../services/locales';

class Locales extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    locales: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveLocales();
  }

  _retrieveLocales = () => {
    const {filters} = this.state;
    all(filters).success(res => {
      this.setState({
        locales: res.locales,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveLocales);
  };

  handleShowSizeChange = (_, per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveLocales);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveLocales)
  };

  prepareToAction = (record, action) => {
    this.setState({
      selectedRecord: record,
      showConfirm: true,
      action: action
    })
  };

  closeConfirm = () => {
    this.setState({showConfirm: false})
  };

  handleDelete = () => {
    const {selectedRecord} = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveLocales();
      this.closeConfirm();
    });
  };

  render() {
    const {isLoading} = this.props.app.main;
    const {locales, showConfirm, count} = this.state;
    const {page, per_page} = this.state.filters;
    const {palette} = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <h2>{ I18n.t('headers.locale') }</h2>
        <Row>
          <Col sm={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10', '20', '50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col sm={4} className="text-right" style={{minHeight: 61}}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36}/>
            <RaisedButton href='#/locale/new' className='pull-right'
                          primary={true} label={ I18n.t('actions.new') }
            />

          </Col>
        </Row>
        <Row>
          <Filters columns={[
            { label: I18n.t('fields.title'), key: 'title', type: 'string' },
            { label: I18n.t('fields.iso2'), key: 'iso2', type: 'string' },
            { label: I18n.t('fields.iso3'), key: 'iso3', type: 'string' },
            {label: 'Active', key: 'active', type: 'boolean'},
          ]} update={this.updateFilters}/>
        </Row>
        
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>
                <SortingTh 
                  update={this.updateFilters}
                  column='title'>{ I18n.t('fields.title') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column='created_at'>{ I18n.t('fields.created_at') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('fields.iso2') }
                >
                  { I18n.t('fields.iso2') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('fields.iso3') }
                >
                  { I18n.t('fields.iso3') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>Actions</TableHeaderColumn>

            </TableRow>
          </TableHeader>
          
          <TableBody displayRowCheckbox={false}>
            {
              locales.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{item.title}</TableRowColumn>
                    {/* <TableRowColumn>{item.active}</TableRowColumn> */}
                    <TableRowColumn>{item.created_at}</TableRowColumn>
                    <TableRowColumn>{item.iso2}</TableRowColumn>
                    <TableRowColumn>{item.iso3}</TableRowColumn>
                    <TableRowColumn className='text-right'>
                    <IconButton onTouchTap={() => location.hash = `#/locale/${item.id}`}>
                        <ActionVisibility color={palette.primary1Color}/>
                      </IconButton>
                      <IconButton onTouchTap={() => location.hash = `#/locale/${item.id}/edit`}>
                        <ImageEdit color={palette.accent1Color} />
                      </IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this, item) }><ActionDelete
                        color="#c62828"/>
                    </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Dialog
          title={ I18n.t('forms.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirm}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handleDelete}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('forms.you_are_going_to_remove') }
        </Dialog>
        <Clearfix/>
      </Paper>
    )
  }
}

Locales.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Locales)
