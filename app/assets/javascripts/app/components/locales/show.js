import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Avatar
} from 'material-ui';
import {paperStyle} from '../common/styles';
import {show} from '../../services/locales';
import PropTypes from "prop-types";

class Locale extends Component {
  state = {
    locale: {
      active: false,
      title: ''
    },
    // id: 0,
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveLocale();
  };


  _retrieveLocale = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        locale: res.locale,
      })
    });
  };


  render() {
    const {locale} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('locale.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href={`#/locales`} className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.title') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {locale.title}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.created_at') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {locale.created_at || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Clearfix/>
      </Paper>
    )
  }
}

Locale.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Locale)