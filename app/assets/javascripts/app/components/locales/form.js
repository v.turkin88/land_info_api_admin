import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix,
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  Toggle,
  AutoComplete
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show, upsert } from '../../services/locales';
import { FormErrorMessage } from '../common/form-error-message.component'


class LocaleForm extends Component {
    state = {
      locale: {
        title: '',
        active: false,
        iso2: '',
        iso3: ''
      },
      validationErrors: {},
    };

  componentWillMount() {
    this._retrieveLocales();
  }

  _retrieveLocales = () => {
    const { id } = this.props.params;
    if (!id) {
      this.setState({
        active: false
      })
      return false
    }
    show(id).success(res => {
      this.setState({
        locale: res.locale,
        active: true
      })
    })
  };

  _handleChange = (key,value) => {
    const { locale } = this.state;

    this.setState({
      locale: {
          ...locale,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };

  _handleSubmit = event => {
    event.preventDefault();
    const { locale } = this.state;
    upsert(locale)
      .success(res => {
        location.hash = '#/locales';
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
          this.setState({
          validationErrors: res.validation_errors
        })
      })
  };

  updateFlag = file => {
    this.setState({
      locale: {
      ...this.state.locale,
        flag: file
      }
    })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { locale, progress, title, validationErrors } = this.state;

    return (
        <Paper style={paperStyle} zDepth={1}>
      <Row>
        <Col sm={6}>
          <h2>{ locale.id ? I18n.t('locale.edit') : I18n.t('locale.new') }</h2>
        </Col>
        <Col sm={6}>
          <RaisedButton
            href='#/locales'
            className='pull-right'
            secondary={true}
            label={ I18n.t('actions.back') }
          />
        </Col>
      </Row>
          <br/>
          <form onSubmit={this._handleSubmit}>
          <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('fields.title') }
                fullWidth={true}
                value={locale.title}
                onChange={(_,val) => this._handleChange('title', val) }
                errorText={ (validationErrors.title || []).join(', ') }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('fields.iso2') }
                fullWidth={true}
                value={locale.iso2}
                onChange={(_,val) => this._handleChange('iso2', val) }
                errorText={ validationErrors.iso2 }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('fields.iso3') }
                fullWidth={true}
                value={locale.iso3}
                onChange={(_,val) => this._handleChange('iso3', val) }
                errorText={ validationErrors.iso3 }
              />
            </FormGroup>
            <FormGroup>
              <Toggle
                label={ I18n.t('fields.active') }
                toggled={locale.active}
                onToggle={() => this._handleChange('active', !locale.active) }
              />
            </FormGroup>
            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label={ I18n.t('actions.submit') } disabled={isLoading} />
            </Col>
            <Clearfix />
          </form>
        </Paper>
    )
  }
}

export default connect(state => state)(LocaleForm)