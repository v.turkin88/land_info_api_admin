import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Row,
    Col,
    ControlLabel,
    FormGroup,
    Clearfix
} from 'react-bootstrap';
import {
    Paper,
    RaisedButton,
    CircularProgress,
    TextField,
    Toggle,
    AutoComplete
} from 'material-ui';
import {paperStyle} from '../common/styles';
import {show, upsert} from '../../services/privacy_policies';
import {FormErrorMessage} from '../common/form-error-message.component'
import {all as getAllLocales} from '../../services/locales';

class PrivacyPolicyForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            privacy_policy: {
                body: '',
                locale: {},
                document_url: ''
            },
            locales: [],
            validationErrors: {}
        }
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount() {
        this._retrievePrivacyPolicy();
        this._retrieveLocaleSearch();
    }

    _retrievePrivacyPolicy = () => {
        const {id} = this.props.params;
        if (!id) {
            return false
        }
        show(id).success(res => {
            res.privacy_policy.created_at = new Date(res.privacy_policy.created_at);
            res.privacy_policy.updated_at = new Date(res.privacy_policy.updated_at);
            this.setState({
                privacy_policy: res.privacy_policy
            })
        })
    };

    _handleChange = (key, value) => {
        const {privacy_policy} = this.state;
        this.setState({
            privacy_policy: {
                ...privacy_policy,
                [key]: value
            },
            validationErrors: {
                ...this.state.validationErrors,
                [key]: null
            }
        }, () => {
            // after state update
        })
    };

    handleChange(value) {
        const {privacy_policy} = this.state;
        this.setState({
            privacy_policy: {
                ...privacy_policy,
                body: value
            }
        })
    }

    _handleSubmit = event => {
        event.preventDefault();
        const {privacy_policy} = this.state;
        upsert(privacy_policy)
            .success(res => {
                location.hash = '#/privacy_policies';
            })
            .progress(value => {
                this.setState({progress: value})
            })
            .error(res => {
                this.setState({
                    validationErrors: res.validation_errors
                })
            })
    };

    updateFlag = file => {
        this.setState({
            privacy_policy: {
                ...this.state.privacy_policy,
                flag: file
            }
        })
    };

    _retrieveLocaleSearch = (value = '') => {
        if (this._localeSearchTimeout) {
            clearTimeout(this._localeSearchTimeout);
        }
        this._localeSearchTimeout = setTimeout(() => {
            getAllLocales({page: 1, per_page: 10}).success(res => {
                this.setState({
                    locales: res.locales
                })
            })
        }, 500);
    };

    modules = {
        toolbar: [
            [{'header': [1, 2, false]}],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
            ['link', 'image'],
            ['clean']
        ],
    }

    formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image'
    ]

    render() {
        const {isLoading} = this.props.app.main;
        const {privacy_policy, progress, validationErrors, locales} = this.state;

        return (
            <Paper style={paperStyle} zDepth={1}>
                <Row>
                    <Col sm={6}>
                        <h2>{privacy_policy.id ? I18n.t('privacy_policy.edit') : I18n.t('privacy_policy.new')}</h2>
                    </Col>
                    <Col sm={6}>
                        <RaisedButton
                            href='#/privacy_policies'
                            className='pull-right'
                            secondary={true}
                            label={I18n.t('actions.back')}
                        />
                    </Col>
                </Row>

                <br/>
                <form onSubmit={this._handleSubmit}>

                    <FormGroup>
                        <AutoComplete
                            searchText={privacy_policy.locale ? privacy_policy.locale.title : ''}
                            errorText={(validationErrors.locale || []).join('. ')}
                            floatingLabelText={I18n.t('fields.locale')}
                            dataSource={locales}
                            fullWidth={true}
                            dataSourceConfig={{text: 'title', value: 'id'}}
                            filter={AutoComplete.caseInsensitiveFilter}
                            openOnFocus={true}
                            onNewRequest={(val) => this._handleChange('locale', val)}
                            onUpdateInput={(val) => this._retrieveLocaleSearch(val)}
                            maxSearchResults={5}
                        />
                    </FormGroup>

                    <FormGroup>
                        <TextField
                            floatingLabelText={I18n.t('fields.document_url')}
                            fullWidth={true}
                            value={privacy_policy.document_url}
                            onChange={(_, val) => this._handleChange('document_url', val)}
                            errorText={(validationErrors.key || []).join(', ')}
                        />
                    </FormGroup>

                    <Col sm={4} smOffset={8} className="text-right">
                        <br/>
                        <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'}
                                          mode="determinate" value={progress} size={36}/>
                        <RaisedButton type='submit'
                                      primary={true}
                                      className='pull-right'
                                      label={I18n.t('actions.submit')}
                                      disabled={isLoading}
                        />
                    </Col>
                    <Clearfix/>
                </form>
            </Paper>
        )
    }
}

export default connect(state => state)(PrivacyPolicyForm)
