import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ControlLabel, Row, Col} 
from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
} from 'material-ui';

import { paperStyle } from '../common/styles';
import { show } from '../../services/privacy_policies';


class PrivacyPolicy extends Component {
  state = {
    privacy_policy: {
      body: '',
      locale: {},
      document_url: '',
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrievePrivacyPolicy();
  }

  _retrievePrivacyPolicy = () => {
    const {id} = this.props.params;
    show(id).success(res => {
      this.setState({
        privacy_policy: res.privacy_policy,
      })
    });
  };

  render() {
    const {privacy_policy} = this.state;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{ I18n.t('privacy_policy.show') }</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href='#/privacy_policies' className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.document_url') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {privacy_policy.document_url || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('fields.locale') }</ControlLabel>
            <br/>
            <span className="form-control-static"> {privacy_policy.locale || '-'}</span>
          </Col>
        </Row>
        <hr/>
      </Paper>
    )
  }
}

export default connect(state => state)(PrivacyPolicy)
