import http from './http';

export function all(filters) {
  let url = '/admin/translations.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model = {}){
  let body = new FormData();

  body.append('content_key_id', model.content_key || '' );
  body.append('locale_id', model.locale_id || '' );
  body.append('title', model.title || '' );

  if (model.content_key) body.append('content_key_id', model.content_key.id);
  if (model.locale) body.append('locale_id', model.locale.id);

  if(model.id){
    return http.put({ url:`/admin/translations/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/translations', body })
  }
}

export function show(id){
  return http.get({url:`/admin/translations/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/translations/${id}`})
}

