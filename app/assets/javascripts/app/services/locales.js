import http from './http';

export function all(filters) {
  let url = '/admin/locales.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model){
  let body = new FormData();

  body.append('title', model.title || '' );
  body.append('active', model.active || false);
  body.append('iso3', model.iso3 || '')
  body.append('iso2', model.iso2 || '')

  if(model.id){
    return http.put({ url:`/admin/locales/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/locales', body })
  }
}

export function show(id){
  return http.get({url:`/admin/locales/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/locales/${id}`})
}

