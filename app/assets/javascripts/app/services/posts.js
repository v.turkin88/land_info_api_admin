import http from './http';

export function all(filters) {
  let url = '/admin/posts.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(post, translates){
  console.log('----------------------')
  let body = new FormData();

  body.append('post[active]', post.active || false );
  body.append('post[status]', post.status || 'draft' );
  
  if(translates){
    let index = 0;
    translates.forEach(item => {
      if (item && !item.id) {
        body.append(`translates[${index}][locale]`, item.locale );
        body.append(`translates[${index}][title]`, item.title || '' );
        body.append(`translates[${index}][description]`, item.description || '');
      }
      index += 1;
    })
  }
  
  if(post.id){
    return http.put({ url:`/admin/posts/${post.id}`, body })
  }else{
    return http.post({ url:'/admin/posts', body })
  }
}

export function show(id){
  return http.get({url:`/admin/posts/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/posts/${id}`})
}
