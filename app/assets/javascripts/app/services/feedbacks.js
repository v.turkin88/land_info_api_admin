import http from './http';

export function all(filters) {
  let url = '/admin/feedbacks.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model){
  let body = new FormData();

  body.append('responce',  model.responce );

  if(model.id){
    return http.put({ url:`/admin/feedbacks/${model.id}/send_mail`, body })
  }else{
    return http.get({ url:'/admin/feedbacks', body })
  }
}

export function show(id){
  return http.get({url:`/admin/feedbacks/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/feedbacks/${id}`})
}
