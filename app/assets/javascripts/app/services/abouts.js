import http from './http';

export function all(filters) {
  let url = '/admin/abouts.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model){
  let body = new FormData();

  body.append('body', model.body || '' );
  body.append('locale_id', model.locale_id || '' );

  if (model.locale) body.append('locale_id', model.locale.id);

  if(model.id){
    return http.put({ url:`/admin/abouts/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/abouts', body })
  }
}

export function show(id){
  return http.get({url:`/admin/abouts/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/abouts/${id}`})
}