import http from './http';

export function all(filters) {
    let url = '/admin/contents.json?';
    Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
    return http.get({url})
}

export function upsert(model){
  let body = new FormData();

  body.append('key', model.key || '' );
  body.append('content_type', model.content_type || '' );
  body.append('data_type', model.data_type || '' );

  if(model.data_type=='string'){
    body.append('body', model.content || '' );
  }else {
    body.append('body', model.body || '' );
  }

  if(model.id){
    return http.put({ url:`/admin/contents/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/contents', body })
  }
}

export function show(id){
  return http.get({url:`/admin/contents/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/contents/${id}`})
}