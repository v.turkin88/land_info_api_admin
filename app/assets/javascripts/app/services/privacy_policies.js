import http from './http';

export function all(filters) {
  let url = '/admin/privacy_policies.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model = {}){
  let body = new FormData();

  body.append('body', model.body || '' );
  body.append('locale_id', model.locale_id || '' );
  body.append('document_url', model.document_url || '' );

  if (model.locale) body.append('locale_id', model.locale.id);



  if(model.id){
    return http.put({ url:`/admin/privacy_policies/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/privacy_policies', body })
  }
}

export function show(id){
  return http.get({url:`/admin/privacy_policies/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/privacy_policies/${id}`})
}