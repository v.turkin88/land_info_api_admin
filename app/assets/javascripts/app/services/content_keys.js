import http from './http';

export function all(filters) {
  let url = '/admin/content_keys.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model){
  let body = new FormData();

  body.append('key', model.key || '' );

  if(model.id){
    return http.put({ url:`/admin/content_keys/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/content_keys', body })
  }
}

export function show(id){
  return http.get({url:`/admin/content_keys/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/content_keys/${id}`})
}
