import http from './http';

export function show() {
  let url = '/admin/term_and_condition.json';
  return http.get({url})
}
export function upsert(model){
  let body = new FormData();

  body.append('term_and_condition[body]', model.body || '' );

  if(model.id){
    return http.put({ url:`/admin/term_and_condition/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/term_and_condition', body })
  }
}