import http from './http';

export function all() {
  let url = '/admin/roles';
  return http.get({url})
}

export function show(id){
  return http.get({url:`/admin/roles/${id}`})
}
