import http from './http';

export function update(data) {
  let body = new FormData();
  // body.append('system_setting[ios_push_environment]', data.ios_push_environment || 'sandbox');
  // body.append('system_setting[ios_push_apns_host]', data.ios_push_apns_host || 'gateway.sandbox.push.apple.com');
  // body.append('system_setting[ios_push_password]', data.ios_push_password || '');
  // body.append('system_setting[apple_purchase_password]', data.apple_purchase_password || '');

  // body.append('system_setting[android_push_token]', data.android_push_token  || '');


  // body.append('system_setting[s3_bucket]', data.s3_bucket  || '');
  // body.append('system_setting[aws_access_key_id]', data.aws_access_key_id  || '');
  // body.append('system_setting[aws_secret_access_key]', data.aws_secret_access_key  || '');
  // body.append('system_setting[s3_region]', data.s3_region  || 'eu-central-1');

  if (data.twilio_account_sid) {
    body.append('system_setting[twilio_account_sid]', data.twilio_account_sid  || '');
  }
  if (data.twilio_auth_token) {
    body.append('system_setting[twilio_auth_token]', data.twilio_auth_token  || '');
  }
  if (data.twilio_phone_number) {
    body.append('system_setting[twilio_phone_number]', data.twilio_phone_number  || '');
  }

  return http.post({ url: '/admin/system_settings', body });
}

export function show() {
  return http.get({ url: '/admin/system_settings' })
}