import http from './http';

export function all(filters) {
  let url = '/admin/faqs.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(faq, translates){
  console.log('----------------------')
  let body = new FormData();

  body.append('faq[active]', faq.active || false );
  
  if(translates){
    let index = 0;
    translates.forEach(item => {
      if (item && !item.id) {
        body.append(`translates[${index}][locale]`, item.locale );
        body.append(`translates[${index}][title]`, item.title || '' );
        body.append(`translates[${index}][description]`, item.description || '');
      }
      index += 1;
    })
  }
  
  if(faq.id){
    return http.put({ url:`/admin/faqs/${faq.id}`, body })
  }else{
    return http.post({ url:'/admin/faqs', body })
  }
}

export function show(id){
  return http.get({url:`/admin/faqs/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/faqs/${id}`})
}
