class Api::V1::PrivacyPoliciesController < Api::V1::BaseController
  skip_before_action :authenticate_user, only: [:index]

  def index
    locale = Locale.locale(params[:locale].downcase)
    @privacy_policy = PrivacyPolicy.find_by(locale_id: locale.id)
  rescue StandardError => e
    render json: { message: 'Locale not found' }, status: :unprocessable_entity and return
  end
end
