class Api::V1::SessionsController < Api::V1::BaseController
  skip_before_action :authenticate_user, except: [:destroy]

  def create
    @user = User.find_by(email: params[:login])

    # render json: { error: 'User not exist. Please check you email for verification' }, status: 404 and return unless @user.present?
    # render json: { error: 'User not verified. Please check you email for verification' }, status: 404 and return unless @user.verified?

    if @user&.authenticate params[:password]
      sign_in user: @user, device_type: params[:device_type], push_token: params[:push_token]

      render json: { session_token: current_session.token, user: current_user.to_sign_up_json }
    else
      render json: { errors: [{ message: 'Email or password is wrong' }] }, status: :bad_request
    end
  end

  def destroy
    sign_out
    render json: { message: 'Logout successful' }
  end

  def update
    @session = current_session
    if @session.update_attributes update_params
      render json: { message: 'Update successful' }
    else
      render json: { errors: @current_session.errors.map { |_k, v| v } }, status: :bad_request
    end
  end

  private

  def login_params
    params.permit(:login, :password)
  end

  def logout_params
    params.permit(:session_token)
  end

  def update_params
    params.permit :push_token, :token
  end
end
