class Api::V1::QuestionsController < Api::V1::BaseController
  skip_before_action :authenticate_user
  load_and_authorize_resource :question

  def show; end

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Question.search_query params
    count_query = Question.search_query params.merge(count: true)

    @questions = Question.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Question.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @question = Question.new question_params.merge(user_id: current_user.id)

    if @question.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @question.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @question.destroy
    render json: { message: I18n.t('messages.success_upsert') }
  end

  def update
    if @question.update question_params
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @question.errors }, status: :unprocessable_entity
    end
  end

  private

  def question_params
    allowed_params = params.permit :title, :description, :status
    allowed_params[:user_id] = current_user.id
    allowed_params
  end

  def create_params
    allowed_params = params.permit :title, :description, :status
    allowed_params[:user_id] = current_user.id
    allowed_params
  end
end
