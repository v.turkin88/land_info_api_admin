class Api::V1::AnswersController < Api::V1::BaseController
  skip_before_action :authenticate_user
  load_and_authorize_resource :answer

  def show
  end

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Answer.search_query params
    count_query = Answer.search_query params.merge(count: true)

    @answers = Answer.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Answer.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @answer = Answer.new answer_params

    if @answer.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @answer.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @answer.destroy
    render json: { ok: true }
  end

  def update
    if @answer.update update_params
        render json: { message: I18n.t('messages.success_upsert') }
    else
        render json: { validation_errors: @answer.errors }, status: :unprocessable_entity
    end
  end

  private

  def answer_params
    allowed_params = params.permit :description, :status, :question_id, :user_id
    allowed_params[:user_id] = current_user.id

    allowed_params
  end

  def update_params
    allowed_params = params.permit :description, :user_id, :status
    # allowed_params[:user_id] = current_user.id

    allowed_params
  end
end
