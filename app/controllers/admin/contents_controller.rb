class Admin::ContentsController < Admin::BaseController

  load_and_authorize_resource :content

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Content.search_query params
    count_query = Content.search_query params.merge(count: true)

    @contents = Content.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Content.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @content = Content.new content_params

    if @content.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @content.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @content.update_attributes content_params
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @content.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @content.destroy
      render json: {message: I18n.t('messages.success_destroy') }
    else
      render json: {errors: @content.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    @locale = Locale.find_by(id: @content.locale_id)
    @content_key = ContentKey.find_by(id: @content.content_key_id)
  end

  private

  def content_params
    allowed_params = params.permit :data, :locale_id, :content_key_id, :active
    allowed_params
  end
end