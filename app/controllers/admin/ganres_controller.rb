class Admin::GanresController < Admin::BaseController
  load_and_authorize_resource :ganre

  def index
    @ganres = Ganre.all
  end

  def create
    @ganre = Ganre.first_or_create ganre_params

    if @ganre.save
      content_key = ContentKey.find_by(key: 'ganres')

      key = ContentKey.first_or_create(key: @ganre.key, parent_id: content_key.id,
                                       is_group: false, active: true)

      if key.save
        translates_params[:translates].each do |data|
          locale = Locale.locale(data[:locale])
          Content.create(locale_id: locale.id, content_key_id: key.id,
                         ganre_id: @ganre.id, data: data[:value], active: true)
        end
      end

        render json: { message: I18n.t("messages.success_upsert") }
      else
        render json: { validation_errors: @ganre.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @ganre.update_attributes ganre_params
      translates_params[:translates].each do |data|
        locale = Locale.locale(data[:locale])
        Content.find_by(locale_id: locale.id, ganre_id: @ganre.id)
               .update_attributes(data: data[:value])
      end

      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @ganre.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    id = @ganre.id
    if @ganre.destroy
      Content.where(ganre_id: id).destroy_all

      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @ganre.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def ganre_params
    params.permit :key, :active
  end

  def translates_params
    allowed_params = params.permit translates: [:locale, :value]
    allowed_params
  end
end
