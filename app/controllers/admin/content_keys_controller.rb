class Admin::ContentKeysController < Admin::BaseController

  load_and_authorize_resource :content_key

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = ContentKey.search_query params
    count_query = ContentKey.search_query params.merge(count: true)

    @content_keys = ContentKey.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = ContentKey.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @content_key = ContentKey.new content_key_params

    if @content_key.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @content_key.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @content_key.update_attributes content_key_params
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @content_key.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @content_key.destroy
      render json: {message: I18n.t('messages.success_destroy') }
    else
      render json: {errors: @content_key.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def content_key_params
    allowed_params = params.permit :key
    allowed_params
  end
end