class Admin::TranslationsController < Admin::BaseController

  load_and_authorize_resource :translation

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Translation.search_query params
    count_query = Translation.search_query params.merge(count: true)

    @translations = Translation.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Translation.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @translation = Translation.new translation_params

    if @translation.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @translation.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @translation.update_attributes translation_params
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @translation.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @translation.destroy
      render json: {message: I18n.t('messages.success_destroy') }
    else
      render json: {errors: @translation.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    @locale = Locale.find_by(id: @translation.locale_id)
    @content_key = ContentKey.find_by(id: @translation.content_key_id)
  end

  private

  def translation_params
    allowed_params = params.permit :locale_id, :content_key_id, :active, :title
    allowed_params
  end
end