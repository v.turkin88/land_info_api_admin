class Admin::AboutsController < Admin::BaseController
  load_and_authorize_resource :about

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = About.search_query params
    count_query = About.search_query params.merge(count: true)

    @abouts = About.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = About.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end

  def create
    @about = About.new about_params

    if @about.save
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @about.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @about.update_attributes about_params
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @about.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @about.destroy
      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @about.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def about_params
    params.permit :body, :locale_id
  end
end
