class Admin::PostsController < Admin::BaseController
  load_and_authorize_resource :post

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Post.search_query params
    count_query = Post.search_query params.merge(count: true)

    @posts = Post.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Post.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end

  def create
    @post = Post.new post_params

    params[:translates].values.each do |body|
      I18n.locale = body[:locale]
      @post.title = body[:title]
      @post.description = body[:description]
    end

    if @post.save
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @post.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @post.update_attributes post_params

      params[:translates].values.each do |body|
        I18n.locale = body[:locale]
        @post.title = body[:title]
        @post.description = body[:description]
      end

      if @post.save
        render json: { message: I18n.t("messages.success_upsert") }
      else
        render json: { validation_errors: @post.errors }, status: :unprocessable_entity
      end
    else
      render json: { validation_errors: @post.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @post.destroy
      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @post.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def post_params
    allowed_params = params.require(:post).permit :active, :status
    allowed_params[:is_admin] = true
    allowed_params[:user_id] = current_user.id
    
    allowed_params
  end
end
