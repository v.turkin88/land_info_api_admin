class Admin::SubjectsController < Admin::BaseController
  load_and_authorize_resource :subject

  def index
    @subjects = Subject.all
  end

  def create
    @subject = Subject.first_or_create subject_params

    if @subject.save
      content_key = ContentKey.find_by(key: 'subject')

      key = ContentKey.first_or_create(key: @subject.key, parent_id: content_key.id,
                                       is_group: false, active: true)

      if key.save
        translates_params[:translates].each do |data|
          locale = Locale.locale(data[:locale])
          Content.create(locale_id: locale.id, content_key_id: key.id,
                         subject_id: @subject.id, data: data[:value], active: true)
        end
      end

      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @subject.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @subject.update_attributes subject_params
      translates_params[:translates].each do |data|
        locale = Locale.locale(data[:locale])
        Content.find_by(locale_id: locale.id, subject_id: @subject.id)
               .update_attributes(data: data[:value])
      end

      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @subject.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    id = @subject.id
    if @subject.destroy
      Content.where(subject_id: id).destroy_all

      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @subject.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def subject_params
    params.require(:subject).permit :key, :active
  end

  def translates_params
    allowed_params = params.permit translates: [:locale, :value]
    allowed_params
  end
end
