class Admin::PrivacyPoliciesController < Admin::BaseController

  load_and_authorize_resource :privacy_policy

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = PrivacyPolicy.search_query params
    count_query = PrivacyPolicy.search_query params.merge(count: true)

    @privacy_policies = PrivacyPolicy.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = PrivacyPolicy.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @privacy_policy = PrivacyPolicy.new privacy_policy_params

    if @privacy_policy.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @privacy_policy.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @privacy_policy.update_attributes privacy_policy_params
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @privacy_policy.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @privacy_policy.destroy
      render json: {message: I18n.t('messages.success_destroy') }
    else
      render json: {errors: @privacy_policy.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def privacy_policy_params
    allowed_params = params.permit :body, :locale_id, :document_url
    allowed_params
  end
end