class Admin::FeedbacksController < Admin::BaseController
  load_and_authorize_resource :feedback

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Feedback.search_query params
    count_query = Feedback.search_query params.merge(count: true)

    @feedbacks = Feedback.includes(:user).find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Feedback.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end

  def destroy
    if @feedback.destroy
      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @feedback.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    @feedback.update_attributes(viewed: true)
  end

  private
end
