class Admin::FaqsController < Admin::BaseController
  load_and_authorize_resource :faq

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Faq.search_query params
    count_query = Faq.search_query params.merge(count: true)

    @faqs = Faq.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Faq.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end

  def create
    @faq = Faq.new faq_params

    params[:translates].values.each do |body|
      I18n.locale = body[:locale]
      @faq.title = body[:title]
      @faq.description = body[:description]
    end

    if @faq.save
      render json: { message: I18n.t("messages.success_upsert") }
    else
      render json: { validation_errors: @faq.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @faq.update_attributes faq_params

      params[:translates].values.each do |body|
        I18n.locale = body[:locale]
        @faq.title = body[:title]
        @faq.description = body[:description]
      end

      if @faq.save
        render json: { message: I18n.t("messages.success_upsert") }
      else
        render json: { validation_errors: @faq.errors }, status: :unprocessable_entity
      end
    else
      render json: { validation_errors: @faq.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @faq.destroy
      render json: { message: I18n.t("messages.success_destroy") }
    else
      render json: { errors: @faq.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def faq_params
    params.require(:faq).permit :active
  end
end
