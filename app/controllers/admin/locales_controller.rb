class Admin::LocalesController < Admin::BaseController

  load_and_authorize_resource :locale

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Locale.search_query params
    count_query = Locale.search_query params.merge(count: true)

    @locales = Locale.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Locale.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @locale = Locale.new locale_params

    if @locale.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @locale.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @locale.update_attributes locale_params
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @locale.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @locale.destroy
      render json: {message: I18n.t('messages.success_destroy') }
    else
      render json: {errors: @locale.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
  end

  private

  def locale_params
    allowed_params = params.permit :iso2, :iso3, :active, :key, :title
    allowed_params
  end
end