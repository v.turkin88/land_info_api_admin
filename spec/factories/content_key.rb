FactoryBot.define do
  factory :content_key do
    abc = ("a".."z").to_a.join
    is_group { false }
    parent_id {nil}
    sequence(:key) { |n| "key_#{abc[n]}" }
  end
end
