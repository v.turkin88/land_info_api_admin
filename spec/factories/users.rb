FactoryBot.define do
  factory :user do
    abc = ("a".."z").to_a.join
    sequence(:email) { |n| "test_#{n}@gmail.com" }
    sequence(:verified) { true }
    sequence(:verified_at) { Time.now }
    sequence(:nickname) { |n| "nick#{abc[n]}" }

    password { 'secret!' }
    # password_confirmation { 'secret!' }

    trait :business do
      sequence(:first_name) { |n| "user#{abc[n]}" }
      sequence(:last_name) { |n| "user#{abc[n]}" }
      role { Role.get_business }
    end

    trait :artist do
      sequence(:first_name) { |n| "user#{abc[n]}" }
      sequence(:last_name) { |n| "user#{abc[n]}" }
      role { Role.get_artist }
    end

    trait :admin do
      role { Role.get_admin }
      sequence(:first_name) { |n| "admin#{abc[n]}" }
      sequence(:last_name) { |n| "admin#{abc[n]}" }
    end
  end
end
