FactoryBot.define do
  factory :subject do
    sequence(:key) { |n| "key#{n}" }
    active { false }
  end
end
