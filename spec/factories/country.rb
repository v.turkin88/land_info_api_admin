FactoryBot.define do
  factory :country do
    selected { false }
    sequence(:phone_code) { |n| n }
    sequence(:name) { |n| "country#{n}" }
    alpha2_code { "US" }
    alpha3_code { "USA" }
    sequence(:numeric_code) { |n| n }
  end
end
