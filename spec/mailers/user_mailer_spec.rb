require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  let(:user) { create :user, :business }

  describe 'welcome_email' do
    let(:mail) { described_class.welcome_email(user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Welcome to LandCare')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['admin@landcare.com'])
    end

    it 'exist verification code' do
      expect(mail.body.encoded).to match(user.verification_code)
    end
  end

  describe 'verification_email' do
    let(:mail) { described_class.verification_email(user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('LandCare. Verification completed.')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['admin@landcare.com'])
    end

    it 'exist verification code' do
      expect(mail.body.encoded).to match('sucessfully verified')
    end
  end

  describe 'confirmation_instructions' do
    let(:mail) { described_class.password_changed(user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('LandCare. Password changed.')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['admin@landcare.com'])
    end

    it 'assigns last_name and name to business ' do
      if user.first_name.present? && user.last_name.present?
        expect(mail.body.encoded).to match(user.first_name)
        expect(mail.body.encoded).to match(user.last_name)
      else
        expect(mail.body.encoded).to match(user.nickname)
      end
    end

    it 'assigns confirmation_token' do
      expect(mail.body.encoded).to match('Your password changed')
    end
  end

  describe 'password_reset' do
    let(:mail) { described_class.password_reset(user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('LandCare. Reset password instructions.')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['admin@landcare.com'])
    end

    it 'assigns name to business ' do
      if user.first_name.present? && user.last_name.present?
        expect(mail.body.encoded).to match(user.first_name)
        expect(mail.body.encoded).to match(user.last_name)
      else
        expect(mail.body.encoded).to match(user.nickname)
      end
    end

    it 'assigns reset_password_token' do
      user.send_password_reset
      user.reload
      expect(user.reset_password_token).not_to be_nil
      expect(mail.body.encoded).to match(user.reset_password_token)
    end
  end
end
