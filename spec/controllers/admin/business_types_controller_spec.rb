require 'rails_helper'

RSpec.describe ::Admin::BusinessTypesController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }
  let(:admin) { create :user, :admin, verified: true }
  let(:business) { create :user, :business, verified: true }
  let(:content_key) { create :content_key }
  let(:business_type_params) do
    {
      content_key_id: content_key.id,
      active: true
    }
  end

  describe 'with success' do
    it 'for index' do
      sign_in user: admin

      BusinessType.first_or_create business_type_params

      get :index

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['business_types'].count).to eq(1)
    end

    it 'for show' do
      sign_in user: admin

      business_type = BusinessType.first_or_create business_type_params

      get :show, params: { id: business_type.id }

      expect(response.status).to be 200
    end

    it 'for create' do
      sign_in user: admin

      post :create, params: business_type_params

      expect(response.status).to be 200
    end

    it 'for destroy' do
      sign_in user: admin

      business_type = BusinessType.first_or_create business_type_params

      delete :destroy, params: { id: business_type.id }

      expect(response.status).to be 200
    end

    it 'for update' do
      sign_in user: admin

      business_type = BusinessType.first_or_create business_type_params

      put :update, params: { id: business_type.id, active: false }

      expect(response.status).to be 200
      expect(BusinessType.find(business_type.id).active).to be false
    end
  end

  describe 'with error' do
    it 'for create' do
      sign_in user: business

      post :create, params: business_type_params

      expect(response.status).to be 401
    end

    it 'for destroy' do
      sign_in user: business

      business_type = BusinessType.first_or_create business_type_params

      delete :destroy, params: { id: business_type.id }

      expect(response.status).to be 401
    end

    it 'for update' do
      sign_in user: business

      business_type = BusinessType.first_or_create business_type_params

      put :update, params: { id: business_type.id, active: false }

      expect(response.status).to be 401
    end
  end
end