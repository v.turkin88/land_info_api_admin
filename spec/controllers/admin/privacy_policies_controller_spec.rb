require 'rails_helper'

RSpec.describe ::Admin::PrivacyPoliciesController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }
  let(:admin) { create :user, :admin }
  let(:business) { create :user, :business }
  let(:artist) { create :user, :artist }
  let(:locale) {create :locale, active: true}

  let(:privacy_policy_params) do
    {
      title: 'Test',
      body: 'Test Body',
      locale_id: locale.id
    }
  end

  describe 'with success' do
    it 'for create' do
      sign_in user: admin
      post :create, params: privacy_policy_params

      expect(response.status).to be 200
      expect(PrivacyPolicy.all.count).to be 1
    end

    it 'for index' do
      sign_in user: admin
      privacy_policy = create :privacy_policy, locale_id: locale.id

      get :index
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)['privacy_policies'].count).to eq(1)
    end

    it 'for update' do
      sign_in user: admin

      privacy_policy = create :privacy_policy, locale_id: locale.id

      expect(PrivacyPolicy.where(id: privacy_policy.id).count).to eq(1)

      put :update, params: { id: privacy_policy.id, body: 'AAAA' }
      expect(response.status).to eq(200)
      expect(PrivacyPolicy.find(privacy_policy.id).body).to eq('AAAA')
    end

    it 'for destroy' do
      sign_in user: admin
      privacy_policy = create :privacy_policy, locale_id: locale.id

      expect(PrivacyPolicy.where(id: privacy_policy.id).count).to eq(1)

      delete :destroy, params: {id: privacy_policy.id}
      expect(response.status).to eq(200)
      expect(PrivacyPolicy.where(id: privacy_policy.id).count).to eq(0)
    end
  end

  describe 'for error' do
    it 'for create' do
      sign_in user: admin

      post :create, params: { body: '' }
      expect(response.status).to be 422
    end

    it 'for artist permission' do
      sign_in user: artist
      post :create, params: privacy_policy_params
      expect(response.status).to be 401
    end

    it 'for business permission' do
      sign_in user: business
      post :create, params: privacy_policy_params
      expect(response.status).to be 401
    end

    it 'for index' do
      get :index

      expect(response.status).to be 400
    end
  end
end
