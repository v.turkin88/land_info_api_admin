# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Admin::EmailSenderController, type: :controller do
  render_views

  before do
    EmailSender.destroy_all
  end

  describe 'with success' do
    let(:response_body) { JSON.parse(response.body) }
    let(:user) { create :user, :admin }

    let(:email_sender_params) do
      {
        address: 'test_address',
        port: '30000',
        domain: 'test_domain',
        authentication: 'test_authenification',
        user_name: 'test_user_name',
        password: 'test_password',
        enable_starttls_autoselected: false
      }
    end

    it 'for create' do
      sign_in user: user

      post :create, params: email_sender_params

      expect(response.status).to be 200
      expect(EmailSender.count).to be(1)
    end

    it 'for index' do
      sign_in user: user

      create :email_sender

      get :index

      expect(response.status).to be 200
      expect(JSON.parse(response.body).count).to eq(1)
    end
  end
end
