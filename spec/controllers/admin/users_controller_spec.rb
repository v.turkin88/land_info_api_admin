# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe Admin::UsersController, type: :controller do
  render_views

  let(:admin) { create :user, :admin, verified: true }

  describe 'with success' do
    it 'for create user' do
      User.where(email: 'testuser@gmail.com').destroy_all

      sign_in user: admin

      post :create, params: {
        email: 'test@gmail.com',
        password: 'secret',
        role: 'admin'
      }

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['message']).to eq('Successfully saved.')
    end

    it 'for user profile' do
      sign_in user: admin

      get :profile
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['email']).to eq(admin.email)
    end

    it 'for list of users' do
      create :user, :business
      create :user, :business
      create :user, :business

      sign_in user: admin

      get :index

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 3
      expect(response_body['count']).to be 3
    end

    it 'for show of user' do
      user = create :user, :business

      sign_in user: admin

      get :show, params: { id: user.id }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['user']['id']).to be user.id
      expect(response_body['user']['first_name']).to eq(user.first_name)
    end

    it 'for destroy user' do
      sign_in user: admin

      user = create :user, :business

      delete :destroy, params: { id: user.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['message']).to eq('Successfully removed.')
      expect(User.where(id: user.id).count).to be 0
    end

    it 'for destroy admin' do
      sign_in user: admin

      user = create :user, :admin

      delete :destroy, params: { id: user.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['message']).to eq('Successfully removed.')
      expect(User.where(id: user.id).count).to be 0
    end
  end

  describe 'with error' do
    it 'for validation errors in create artist ' do
      sign_in user: admin

      post :create, params: {
        email: 'test_user@gmail.com',
        password: 'secret',
        password_confirmation: 'secret',
        role: 'artist'
      }

      expect(response.status).to be(422)
      response_data = JSON.parse(response.body)['validation_errors']['nickname'][0]
      expect(response_data).to eq("can't be blank")
    end

    it 'for validation error for already registered email' do
      user = create :user, :business

      sign_in user: admin

      post :create, params: {
        email: user.email,
        password: 'secret',
        password_confirmation: 'secret',
        role: 'business'
      }

      expect(response.status).to be(422)
      response_data = JSON.parse(response.body)['validation_errors']['email'][0]
      expect(response_data).to eq('USER_EMAIL_VALIDATION_UNIQ')
    end

    it 'for destroy user' do
      user = create :user, :business

      delete :destroy, params: { id: user.id }

      expect(response.status).to be 400
      expect(JSON.parse(response.body)['errors'][0]['message']).to eq('Access denied.')
    end

    it 'for destroy all admins' do
      sign_in user: admin

      delete :destroy, params: { id: admin.id }

      expect(response.status).to be 422
      expect(JSON.parse(response.body)['errors'][0]).to eq('Can not remove last admin.')
    end

    it 'for user profile' do
      user = create :user, :admin
      create :user, :admin

      sign_in user: user

      User.find(user.id).destroy

      get :profile
      expect(response.status).to be 400
    end
  end
end
