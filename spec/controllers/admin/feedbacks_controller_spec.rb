# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe ::Admin::FeedbacksController, type: :controller do
  render_views

  let(:admin) { create :user, :admin }
  let(:business) { create :user, :business }
  let(:subject) { create :subject, active: true }

  let(:valid_params) do
    {
      email: business.email,
      user_id: business.id,
      subject_id: subject.id,
      full_name: business.nickname,
      message: 'Test'
    }
  end

  describe 'with success' do
    it 'for index' do
      Feedback.create valid_params

      sign_in user: admin

      get :index

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['feedbacks'].count).to be(1)
    end

    it 'for destroy' do
      feedback = Feedback.create valid_params

      sign_in user: admin

      delete :destroy, params: {id: feedback.id}

      expect(response.status).to be(200)
    end
  end

  describe 'with error' do
    it 'for index' do
      Feedback.create valid_params

      sign_in user: business

      get :index

      expect(response.status).to be(401)
    end

    it 'for destroy' do
      feedback = Feedback.create valid_params

      sign_in user: business

      delete :destroy, params: {id: feedback.id}

      expect(response.status).to be(401)
    end
  end
end
