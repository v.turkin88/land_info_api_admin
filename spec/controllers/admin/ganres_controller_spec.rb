require 'rails_helper'

RSpec.describe ::Admin::GanresController, type: :controller do
  render_views

  describe 'with success' do

    let(:admin) { create :user, :admin }
    let(:response_body) { JSON.parse(response.body) }
    let(:locale) { create :locale, active: true }
    let(:valid_params) do
      {
        "key": "aaa",
        "active": true,
      }

    end

    it 'for update' do
      sign_in user: admin

      ganre = Ganre.first_or_create valid_params

      put :update, params: {id: ganre.id, key: 'bbb' }

      expect(response.status).to be 200
      expect(Ganre.find(ganre.id).key).to eq('bbb')
    end

    it 'for create' do
      sign_in user: admin

      post :create, params: valid_params

      expect(response.status).to be 200
    end

    it 'for destroy' do
      sign_in user: admin

      ganre = Ganre.first_or_create valid_params

      delete :destroy, params: { id: ganre.id }

      expect(response.status).to be 200

    end

    describe 'with success' do
      it 'for index when authorized user' do
        sign_in user: admin

        ganre = Ganre.first_or_create valid_params

        Content.create(ganre_id: ganre.id, data: 'test', locale_id: locale.id)

        get :index, params: { locale: locale.iso3 }
        expect(response.status).to be 200
        expect(JSON.parse(response.body)['ganres'].count).to eq(1)
      end

    end
  end
end