# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Admin::AboutsController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }
  let(:admin) { create :user, :admin }
  let(:business) { create :user, :business }
  let(:locale) { create :locale, active: true }
  let(:valid_params) do
    {
      "ios_app": "test_ios",
      "ios_app_url": "test_ios_url",
      "android_app": "test_android",
      "android_app_url": "test_android_url",
      "body": "Test body",
      "locale_id": locale.id
    }
  end

  describe 'with success' do
    before do
      sign_in user: admin
    end

    it 'for create' do
      post :create, params: valid_params

      expect(response.status).to be 200
      expect(About.all.count).to be 1
    end

    it 'for index' do
      About.first_or_create valid_params

      get :index
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['abouts'].count).to eq(1)
    end
  end

  describe 'for error' do
    it 'for create' do
      sign_in user: admin

      post :create, params: { body: '' }
      expect(response.status).to be 422
    end

    it 'for create with wrong permission' do
      sign_in user: business
      post :create, params: valid_params
      expect(response.status).to be 401
    end

    it 'for index' do
      About.first_or_create valid_params

      sign_in user: business

      get :index

      expect(response.status).to be 401
    end

    it 'for index with wrong permission' do
      About.first_or_create valid_params

      get :index

      expect(response.status).to be 400
    end
  end
end
