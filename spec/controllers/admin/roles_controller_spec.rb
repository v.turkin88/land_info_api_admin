require 'rails_helper'

RSpec.describe ::Admin::RolesController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }
  let(:admin) { create :user, :admin, verified: true }
  let(:business) { create :user, :business, verified: true }
  let(:role_business) { Role.get_business }

  describe 'with success' do
    it 'for show' do
      sign_in user: admin

      get :show, params: { id: role_business.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['role']['name']).to eq(role_business.name)
      expect(JSON.parse(response.body)['role']['id']).to eq(role_business.id)
    end

    it 'for update' do
      sign_in user: admin

      put :update, params: { id: role_business.id, role: { title: 'aaaa', to_show: false } }

      role = Role.find(role_business.id)

      expect(response.status).to be 200
      expect(role.title).to eq('aaaa')
      expect(role.to_show).to eq(false)
    end

    it 'for index' do
      sign_in user: admin

      get :index

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['roles'].count).to eq(1)
    end

    it 'for index by name' do
      sign_in user: admin

      get :index, params: { name: role_business.name }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['roles'].count).not_to be_nil
    end

    it 'for index paginate' do
      sign_in user: admin

      get :index, params: { per_page: 1, page: 1, sort_column: 'name', sort_type: 'desc' }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['roles'].count).to eq(1)
    end

    it 'for index and name not exist' do
      sign_in user: admin

      get :index, params: { name: "NoName" }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['roles'].count).to eq(0)
      expect(JSON.parse(response.body)['count']).to eq(0)
    end
  end

  describe 'for error' do
    it 'for show' do
      sign_in user: business

      get :show, params: { id: role_business.id }
      expect(response.status).to be 401
    end

    it 'for show' do
      get :show, params: { id: -100 }

      expect(response.status).to be 400
    end

    it 'for index' do
      sign_in user: business

      get :index

      expect(response.status).to be 401
    end

    it 'for index' do
      get :index

      expect(response.status).to be 400
    end
  end
end
