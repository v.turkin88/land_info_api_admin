def seed_admin
  User.destroy_all
  
  User.create email: "admin@gmail.com",
              password: "secret",
              role: Role.get_admin,
              first_name: "Admin",
              last_name: "LandCase"
end
