def seed_locales
  Locale.destroy_all

  locales_attributes = [
    { iso2: "ua", iso3: "ukr", active: true, title: "ukr" },
    { iso2: "en", iso3: "eng", active: true, title: "eng" },
  ]

  locales_attributes.each do |locale|
    Locale.create locale
  end
end
