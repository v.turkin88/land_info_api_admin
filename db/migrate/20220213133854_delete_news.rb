class DeleteNews < ActiveRecord::Migration[5.1]
  def change
    drop_table(:news, if_exists: true)
  end
end
