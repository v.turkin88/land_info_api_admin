class CreatePrivacyPolicy < ActiveRecord::Migration[5.1]
  def change
    create_table :privacy_policies do |t|
      t.text :body
      t.bigint :locale_id, null: false
      t.string :document_url

      t.timestamps
    end

    add_index :privacy_policies, :locale_id, unique: true
  end
end
