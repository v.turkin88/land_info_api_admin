class DeleteFaqs < ActiveRecord::Migration[5.1]
  def change
    drop_table(:faqs, if_exists: true)
  end
end
