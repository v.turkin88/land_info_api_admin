class CreateFeedbackResponse < ActiveRecord::Migration[5.1]
  def change
    create_table :feedback_responses do |t|
      t.text :body, null: false
      t.bigint :feedback_id, null: false
      t.bigint :user_id, null: false

      t.timestamps
    end
  end
end
