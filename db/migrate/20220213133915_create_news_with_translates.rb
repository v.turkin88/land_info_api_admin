class CreateNewsWithTranslates < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.integer :status, default: 0, null: false
      t.boolean :is_admin, default: false
      t.boolean :active, default: false
      t.integer :user_id

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        Post.create_translation_table! :title => {:type => :string, :null => false, :default => '-'}, :description => {:type => :text, :null => false, :default => '-'} 
      end

      dir.down do
        Post.drop_translation_table!
      end
    end
  end
end
