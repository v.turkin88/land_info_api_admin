class AddUniqueToTranslations < ActiveRecord::Migration[5.1]
  def change
    add_index :translations, [:locale_id, :content_key_id], unique: true

  end
end
