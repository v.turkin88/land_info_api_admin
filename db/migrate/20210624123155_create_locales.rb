class CreateLocales < ActiveRecord::Migration[5.1]
  def change
    create_table :locales do |t|
      t.string :title
      t.string :iso2, nil: false, unique: true
      t.string :iso3, nil: false, unique: true
      t.boolean :active, default: false

      t.timestamps
    end
  end
end
