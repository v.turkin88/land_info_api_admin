class CreateTranslations < ActiveRecord::Migration[5.1]
  def change
    create_table :translations do |t|
      t.string  :title, nil: false
      t.integer :content_key_id, nil: false
      t.integer :locale_id, nil: false
      t.boolean :active, default: false

      t.timestamps
    end
  end
end
