class AddVerificationAndResetPasswordToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :verified_at, :datetime
    add_column :users, :reset_password_at, :datetime
  end
end
