class CreateFaqWithTranslates < ActiveRecord::Migration[5.1]
  def change
    create_table :faqs do |t|
      t.boolean :active, default: false
      t.timestamps
    end

    reversible do |dir|
      dir.up do
        Faq.create_translation_table! :title => {:type => :string, :null => false, :default => '-'}, :description => {:type => :text, :null => false, :default => '-'} 
      end

      dir.down do
        Faq.drop_translation_table!
      end
    end
  end
end
