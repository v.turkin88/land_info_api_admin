class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :encrypted_password
      t.string :salt
      t.string :email
      t.string :first_name
      t.string :last_name
      t.references :role
      t.datetime :last_logged_in

      t.timestamps
    end
  end
end
