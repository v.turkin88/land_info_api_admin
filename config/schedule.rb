set :environment, 'development'
set :output, 'log/cron.log'

every 5.minutes do
  runner "TaskNotificationWorker.perform_async"
end

every 1.minutes do
  runner "PaymentPushNotificationWorker.perform_async"
end

every 2.minutes do
  runner "PaymentStatusWorker.perform_async"
end
