require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module IdyslexicBackend
  class Application < Rails::Application
    config.i18n.enforce_available_locales = false
    config.i18n.available_locales = [:en, :ua]
    config.i18n.default_locale = :en
    config.assets.initialize_on_precompile = true
    config.browserify_rails.commandline_options = '-t babelify --fast'
    config.assets.paths << Rails.root.join('node_modules')
    config.time_zone = 'UTC'
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.active_record.schema_format = :sql

    config.action_cable.allowed_request_origins = [/.*/]
    config.action_cable.disable_request_forgery_protection = true
    config.autoload_paths += %W( #{config.root}/app/streamers )
    config.autoload_paths += %W( #{config.root}/app/workers )
    # add custom validators path
    config.autoload_paths += %W["#{config.root}/app/validators/"]

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
