begin
  Twilio.configure do |config|
    config.account_sid = SystemSetting.twilio_account_sid
    config.auth_token = SystemSetting.twilio_auth_token
  end
rescue
end