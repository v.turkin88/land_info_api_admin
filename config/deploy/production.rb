server  '31.131.18.57',
        port: 22,
        user: 'root',
        roles: [:web, :app, :db], primary: true


# ssh_options[:forward_agent] = true
# set :ssh_options, {:forward_agent => true}
stage = 'production'
set :application, 'landcare'
set :bundle_flags,      '--quiet' # this unsets --deployment, see details in config_bundler task details
set :bundle_path,       nil
set :bundle_without,    nil
set :repo_url, 'git@gitlab.com:viholovko/CloobinBackClassic.git'
set :branch, 'master'
set :stage, stage
set :nvm_node, 'v8.5.0'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/landcare/'

set :linked_files,
    fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs,
    fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache',
                                 'tmp/sockets', 'public/system')

namespace :deploy do
  desc 'Config bundler'
  task :config_bundler do
    on roles(/.*/) do
      within release_path do
        execute :bundle, :config, '--local deployment true'
        execute :bundle, :config, '--local without "development test"'
        execute :bundle, :config, "--local path #{shared_path.join('bundle') }"
      end
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
    end
  end
end

before 'bundler:install', 'deploy:config_bundler'