lock "~> 3.14.0"

set :application, "landcare"
set :repo_url, "git@gitlab.com:viholovko/CloobinBackClassic.git"

set :deploy_to, '/var/www/landcare'
set :use_sudo, true

set :branch, 'master' #or whichever branch you want to use
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
set :linked_files, fetch(:linked_files, []).push('config/database.yml','config/secrets.yml')

set :ssh_options, {
  forward_agent: true,
}
